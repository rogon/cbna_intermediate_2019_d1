# Based on tutorials and slides by Aaron Brooks, Jean-Karim Heriche, Joao Neto, Matt Rogon
# The R code was obtained and adapted from João Neto
# http://www.di.fc.ul.pt/~jpn/r/spectralclustering/spectralclustering.html

list.of.packages <- c("ggplot2", "pheatmap", "dplyr", "reshape2", "igraph", "linkcomm", "httr", "kernlab")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[, "Package"])]
if(length(new.packages)) install.packages(new.packages, dependencies = T)

https://raw.githubusercontent.com/scalefreegan/Teaching/master/DataIntegration/scripts/readData.R
#! /usr/bin/env Rscript
# designed to be opened as
# devtools::source_url("https://raw.githubusercontent.com/scalefreegan/Teaching/master/DataIntegration/scripts/readData.R")

setwd("/Volumes/Work/01. Teaching/CBNA Courses/Level 2-3 Graph Theory, Advanced scripting and automation/2019/Day 1 - centralities and modularity/2. Network clustering and modularity/Spectral Clustering in R/Ens78")

# note that the data is no longer available on the servers - it's local in the folder ens78 - you need to modify the scripts to reflect the new location
# devtools::source_url("https://raw.githubusercontent.com/scalefreegan/Teaching/master/DataIntegration/scripts/readData.R")

#-------------------------------------------------------------------#
# Process Course Data into a computable format
#-------------------------------------------------------------------#

# Import packages ---------------------------------------------------
library(ggplot2)
library(dplyr)
library(reshape2)

# Dir infos ---------------------------------------------------
FDIR = "https://oc.embl.de/index.php/s/qiOSCyvYRdxraRw/download?path=%2F&files="
GITHUBDIR = "http://scalefreegan.github.io/Teaching/DataIntegration/data/"

# Read ---------------------------------------------------

readData = function(f, fnames, URL) {
  # f is vector of file paths
  # fnames is vector of names for theses files in final data.frame
  # Data files are 3 column, tab-delimited, gene1:gene2:score
  pb <- txtProgressBar(min = 0, max = length(f), style = 3)
  o = lapply(seq(1,length(f)),function(i){
      setTxtProgressBar(pb, i)
      d = read.table(url(paste(FDIR,f[i],sep=""), method = "libcurl"), sep = "\t", stringsAsFactors = F)
      if (ncol(d) == 2) {
        d = cbind(d,1)
      }
      colnames(d) = c("gene1","gene2",fnames[i])
      # needs sorting
      tosort = d[,1]>d[,2]
      d[tosort, c("gene1","gene2")] = d[tosort, c("gene2","gene1")]
      d = d[with(d, order(gene1,gene2)),]
      d2 = data.frame(paste(d[,"gene1"], d[,"gene2"], sep = "_"), d[, fnames[i]], stringsAsFactors = F)
      colnames(d2) = c("genes", fnames[i])
      return(d2)
    })
  out = o[[1]]
  if (length(o) > 1) {
    for (i in 2:length(o)) {
      #print(i)
      out = merge(out, o[[i]], by = "genes", all = TRUE, , stringsAsFactors = F)
    }
  }
  # clean up non-symmetric entries
  r = names(which(table(out$genes) > 1))
  if (length(r) > 0) {
    for (i in r) {
      inds = which(out$genes == i)
      vals = apply(out[inds,2:dim(out)[2]], 1, sum, na.rm=T)
      tor = inds[which(vals == min(vals))[1]]
      out = out[-tor,]
    }
  }
  # split genes into gene1 gene2
  tor = do.call(rbind, strsplit(out$genes, split = "_"))
  tor = cbind(tor, out[, 2:dim(out)[2]],stringsAsFactors = F)
  colnames(tor)[1:2] = c("gene1", "gene2")
  close(pb)
  return(tor)
}

# Read data ---------------------------------------------------
f_full = c(
  "BP_Ens78.txt",
  "CODA80_Ens78.txt",
  "HIPPO_Ens78.txt",
  "PI_Ens78.txt",
  "TM_Ens78.txt"
  )

f_reduced = c(
  "BP_reduced_data.txt",
  "CODA80_reduced_data.txt",
  "HIPPO_reduced_data.txt",
  "PI_reduced_data.txt",
  "TM_reduced_data.txt"
  )

f_names = c(
  "BP_resnik",
  "CODA80",
  "HIPPO",
  "PI",
  "TM"
  )

f_data = paste(GITHUBDIR, "data.rda", sep="")
if (!httr::url_success(f_data)) {
  #data_full = readData(f_full, f_names)
  # data_reduced = readData(f_reduced, f_names)
  data = readData(f_reduced, f_names)
} else {
  # why not update?
  load(url(f_data))
}

# Characterize data ---------------------------------------------------

#The data is returned as a 11060 by 7 data.frame with the following structure:
head(data)
##             gene1           gene2 BP_resnik CODA80 HIPPO PI       TM
## 1 ENSG00000000971 ENSG00000132693        NA     NA    NA  1 33.33333
## 2 ENSG00000001626 ENSG00000102189        NA     NA    NA  1       NA
## 3 ENSG00000001626 ENSG00000114520        NA     NA    NA  1       NA
## 4 ENSG00000001626 ENSG00000137642        NA     NA    NA  1       NA
## 5 ENSG00000001626 ENSG00000174371        NA     NA    NA  1       NA
## 6 ENSG00000002016 ENSG00000010292        NA     NA    NA  1       NA


#Combine and normalize kernels
# A valid kernel should be positive semidefinite (i.e., a Hermitian matrix whose 
# eigenvalues are nonnegative). One way to make sure that our kernels are positive 
# semidefinite is to add a sufficiently large value to the diagonal. 
# For example, you can add the absolute value of the smallest eigenvalue to the diagonal.
# Once the validity of the kernels is established, each of them can simply be added 
# together to produce a combined kernel.

# Combine and normalize the kernels into a single matrix with values on the interval 
# \([0,1]\), verifying that each is a valid kernel.
# Return a composite matrix that has dimensions 4567 by 4567
# To save some effort, here is a function to normalize a matrix, \(K\), 
# on the interval \([0,1]\).

normalizeKernel = function(K) {
  # from Shawe-Taylor & Cristianini's "Kernel Methods for Pattern Analysis", p113
  # original kernel matrix stored in variable K
  # output uses the same variable K
  # D is a diagonal matrix storing the inverse of the norms
  # Based on MATLAB script from: www.kernel-methods.net
  rnames = rownames(K)
  cnames = colnames(K)
  D = diag(1/sqrt(diag(K)))
  K = D %*% K %*% D
  rownames(K) = rnames
  colnames(K) = cnames
  return(K)
}

# Solution

library(rARPACK) # for function eigs, Thanks Jean-Karim!
ms = lapply(colnames(data)[3:7], function(d){
  z = select(data, gene1, gene2, which(colnames(data)==d))
  z[is.na(z[,d]),d] = 0
  g = sort(unique(c(z$gene1, z$gene2)))
  m = matrix(0, nrow = length(g),ncol = length(g), dimnames = list(g,g))
  m[cbind(z$gene1, z$gene2)] = z[,d]
  m[cbind(z$gene2, z$gene1)] = z[,d]

  # calc smales eigenvalue
  eigen_m <- eigs(m,1,which="SR") # compute the smallest eigenvalue and corresponding eigenvector

  # make sure m is a valid kernel by adding
  # make matrix positive semi-definite
  toadd = ceiling(abs(min(eigen_m$values)))
  diag(m) = diag(m) + toadd
  # you don't have to normalize every kernel individually
  nm = normalizeKernel(m)
  o = list()
  o$m = m
  o$nm = nm
  return(o)
})

mc = ms[[1]]$nm
for (i in 2:length(ms)) {
  mc = mc + ms[[i]]$nm
}
mc = normalizeKernel(mc)



# Make a graph
# Now that we have transformed the similarity kernels into a single combined matrix, we can visualize it as a graph using the R-package igraph.
# Plotting the graph can take some time, so I’ve simply included an image of it below. The default igraph layout for a graph of this size is the DrL layout, which is a force-directed layout suitable for larger graphs (# vertices > 1000). The resulting image gives us a first clue about how we might cluster the network.

library(igraph)
g = graph_from_adjacency_matrix(mc, mode = "undirected", weighted = TRUE, diag = FALSE)

plot.igraph(g, edge.width = E(g)$weight/(max(E(g)$weight)/5), vertex.label = NA, vertex.size = 5, edge.curved = T)


#----------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------
# Spectral Clustering
#----------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------

# libraries
library(pheatmap)
library(dplyr)
library(reshape2)
library(igraph)
library(linkcomm)
library(kernlab)
# data
data(spirals)

#To remind you, spectral clustering appeared to provide a more desired result when clustering data sets that are separated poorly by standard k-means. Below I plot the data and the clustering result for both algorithms.
#In what follows, we will write an algorithm to perform spectral clustering. We will see - perhaps surprisingly - that spectral clustering boils down to performing k-means on eigenvectors of a similarity kernel applied to the original data.
#To help you write your code, João Neto has provided several pre-rolled functions, including:
#A Gaussian kernel, s, to calculate the similarity between two points.

s <- function(x1, x2, alpha=1) {
  exp(- alpha * norm(as.matrix(x1-x2), type="F"))
}

#A function, make.affinity to compute a restricted (or filtered) “affinity” between vertices using k-nearest neighbors.

make.affinity <- function(S, n.neighboors=2) {
  N <- length(S[,1])

  if (n.neighboors >= N) {  # fully connected
    A <- S
  } else {
    A <- matrix(rep(0,N^2), ncol=N)
    for(i in 1:N) { # for each line
      # only connect to those points with larger similarity
      best.similarities <- sort(S[i,], decreasing=TRUE)[1:n.neighboors]
      for (s in best.similarities) {
        j <- which(S[i,] == s)
        A[i,j] <- S[i,j]
        A[j,i] <- S[i,j] # to make an undirected graph, ie, the matrix becomes symmetric
      }
    }
  }
  A
}

# Your algorithm should include the following steps:
#    Compute the similarity matrix, S, between all points in the spiral dataset
#    Calculate the affinity matrix, A, from S by applying k-nearest neighbors algorithm
#    Compute the weighted degree diagonal matrix, D, from A by summing across each row
#    Calculate the unnormalized Laplacian, U, by subtracting A from D
#    Compute eigenvectors and eigenvalues of U.
#    Perform k-means clustering on k smallest eigenvalues, ignoring the smallest (constant) eigenvector

#We will perform these steps one at a time. If you find yourself spending more than 10-15 minutes on this section, please read the solution and move on to the following activities.
#        Compute the similarity matrix, S, between all points in the spiral dataset

  make.similarity <- function(my.data, similarity) {
    N <- nrow(my.data)
    S <- matrix(rep(NA,N^2), ncol=N)
    for(i in 1:N) {
      for(j in 1:N) {
        if (i!=j) {
          S[i,j] <- similarity(my.data[i,], my.data[j,])
        } else {
          S[i,j] <- 0
        }
      }
    }
    S
  }

  S <- make.similarity(spirals, s)


# Calculate the affinity matrix, A, from S by applying k-nearest neighbors algorithm
A <- make.affinity(S, 3)  # use 3 neighbors (includes self)

# Compute the weighted degree diagonal matrix, D, from A by summing across each row
D <- diag(apply(A, 1, sum))

# Calculate the unnormalized Laplacian, U, by subtracting A from D
U <- D - A

# Compute eigenvectors and eigenvalues of U.
evL <- eigen(U, symmetric=TRUE)

# Perform k-means clustering on matrix, Z, consisting of eigenvectors for k smallest eigenvalues, ignoring the smallest (constant) eigenvector
k   <- 2
Z   <- evL$vectors[,(ncol(evL$vectors)-k+1):ncol(evL$vectors)]
km <- kmeans(Z, centers=k, nstart=5)

# Plot the eigenvector matrix (Z) as well as the original data Are the two spiral clusters correctly identified? What does the data look like in the transformed space?
par(mfrow=c(1,2))
plot(Z, col=km$cluster, pch = 19, xlab = "Eigenvector 1", ylab = "Eigenvector 2")
plot(spirals, col=km$cluster, pch = 19, xlab = "", ylab = "")

# You may have noticed a parameter or two lurking around, for example the n.neighboors parameter in the k-nearest neighbors function. These parameters would need to be adjusted depending on the data or estimated automatically. In addition, it should be noted that there are several implementations of spectral clustering, using any of a number of different laplacians - you have just implemented a relatively simple version for the sake of clarity. For more detailed information, please refer to Ng et al. On Spectral Clustering: Analysis and an algorithm
# Load Kernel Data
# Load the combined kernel and graph produced in the preceding practical

GITHUBDIR = "http://scalefreegan.github.io/Teaching/DataIntegration/data/"
load(url(paste(GITHUBDIR, "kernel.rda", sep = "")))

#location of the file: /Volumes/Work/01. Teaching/CBNA Courses/Level 4 Integrative course CBNA with Jean-Karim, Aaron (2015)/-=- Aaron -=-/Teaching-master/DataIntegration/data/kernel.rda


# Spectral Clustering: subset
# Before clustering the full dataset - which can be hard to visualize and evaluate - we will apply spectral clustering on a representative subset of the graph so that we can visualize its behavior.
# I have selected 4 vertices with fairly high degree (degree = 20) from the matrix, mc, and selected all of their first neighbors. The sub-matrix is called, mc2.

degree = apply(mc, 1, function(i)sum(i > 0))
n_i = which(degree == 20)[1:4]
n = unique(c(n_i, which(mc[n_i, ] > 0, arr.ind = T)[, 2]))
mc2 = mc[n, n]

# Make an igraph network from the sub-matrix mc2.
# Plot the graph. How many clusters do you expect based on this visualization?

g_small = graph_from_adjacency_matrix(mc2, mode = "undirected", weighted = TRUE, diag = FALSE)
g_layout = layout.fruchterman.reingold(g_small)
plot.igraph(g_small, vertex.label = NA, layout = g_layout)

# Perform spectral clustering on the sub-matrix mc2. Remember that the matrix is a kernel. Therefore, you should pass it to corresponding functions using the function as.kernelMatrix()
# Spectral clustering can be performed with the function specc from the kernlab package. Choose the number of clusters (centers) to be equal to the number you estimated above.
# Make an annotation data.frame, v_small, that contains a single column denoting the cluster membership of each gene. The values should be encoded as factors. The rownames of this matrix should correspond to the rownames of mc2.

mc2_specc = specc(as.kernelMatrix(mc2), centers = 3)
v_small = as.data.frame(factor(mc2_specc))
rownames(v_small) = rownames(mc2)

# Make an igraph network from the kernel matrix, mc2.
# Plot the network, coloring each node by cluster membership as contained in the data.frame, v.
# How do the clusters correspond to those you predicted before clustering?

g_small = graph_from_adjacency_matrix(mc2, mode = "undirected", weighted = TRUE, diag = FALSE)
plot.igraph(g_small,vertex.label=NA,vertex.color=v_small[names(V(g_small)), ], layout = g_layout)

# Plot a heatmap representation of the kernel matrix, mc2. Annotate each column (gene) with its corresponding cluster membership. A nice package for plotting heatmaps is the pheatmap package, which allows you to annotate both the rows and the columns of a matrix.
# Set the diagonal of mc2 = 0, so that the low-weighted edges are better represented.
# Are there any differences you notice between this representation and the previous graph-based visualization?



diag(mc2) = 0
pheatmap(mc2[rownames(v_small)[order(v_small[,1])],rownames(v_small)[order(v_small[,1])]], annotation = v_small, show_rownames = F, show_colnames = F, cluster_rows = F, cluster_cols = F)

#Spectral Clustering: full
#Having evaluated the behavior of spectral clustering on a small example, we will now apply it to the full data set.
#Perform spectral clustering on the full kernel matrix, mc. As before, remember to pass it .asKernelMatrix().
#For this dataset, set parameter centers = 17.
#As before, make an annotation data.frame, v, to store each of the cluster assignments
#Plot the graph, coloring the nodes according to their cluster membership

mc_specc = specc(as.kernelMatrix(mc), centers = 17)
v = as.data.frame(factor(mc_specc))
rownames(v) = rownames(mc)
colors = c(RColorBrewer::brewer.pal(9,"Set1"),RColorBrewer::brewer.pal(9,"Set3"),RColorBrewer::brewer.pal(8,"Set2"))
plot.igraph(g,vertex.label=NA,vertex.color=v[names(V(g)), ], edge.curved = T, vertex.size = 7, palette = colors)

# You will notice that I have provided you with the number of clusters (or centers) to detect in the dataset. In the following lecture and tutorial you will see how I selected this value.


# Link-community detection
## As an alternative to node-based clustering, we will conclude by performing link-community detection on the gene-gene similarity kernel.

# As you will recall from the lecture, link-community detection inverts the clustering problem. Rather than cluster similar nodes, it instead tries to clusters similar edges.
# Interestingly: Given this formulation, nodes can now belong to several clusters. This is particularly useful in biological contexts, where an investigator might like to express, for example, how the role or function of a particular gene may vary across different contexts.

# You can learn more about link-community detection in the original paper
#    Perform link-community clustering on the full kernel matrix, mc, using the function getLinkCommunities() from the linkcomm package.
#    To pass the the kernel matrix to link community algorithm, you will need to format it as an n x 3 edge list matrix, i.e., [node1, node2, weight], where n is the number of pairs of nodes in the network. An easy way to get this matrix is from the igraph graph object g that we created previously using the function as_edgelist()
#    Be sure to set the directed = FALSE for link community clustering.


# link communities
mc_edge = as_edgelist(g)
mc_edge = data.frame(node1 = mc_edge[,1], node2 = mc_edge[,2], weight = E(g)$weight)
g_linkcomm = getLinkCommunities(mc_edge, directed = FALSE, plot = FALSE)

# Unlike spectral clustering, the link-community detection algorithm will automatically select the number of clusters. In the next lecture and practical session we will follow up on both networks, learn how the number of clusters was determined for both approaches.
# For now, you can rest easy.



# Evaluating clustering results by optimizing K number of clusters
# elbow method
Wk = unlist(lapply(seq(2,10),function(i){
  km = kmeans(cbind(iris$Sepal.Length, iris$Sepal.Width), centers = i)
  return( 1-km$tot.withinss/km$totss )
}))
plot(seq(2,10), Wk, type = "l", xlim = c(1,10), lty = 1, xlab = "# Clusters", ylab = "% Variance Explained (1 - tot.withinss/totss)")
points(seq(2,10), Wk, pch = 19)

# Spectral gap
plot(seq(1:10), rev(evL$values)[1:10]+1e-12, pch=8, cex = 1.25, main = "Spirals eigenvalue spectrum: 10 lowest", xlab = "Index", ylab = "Value", log="y")

#Modularity maximization
# The code for computing mc2 is included below for reference, as well as its representation as a network.
degree = apply(mc, 1, function(i)sum(i > 0))
n_i = which(degree == 20)[1:4]
n = unique(c(n_i, which(mc[n_i, ] > 0, arr.ind = T)[, 2]))
mc2 = mc[n, n]

g_small = graph_from_adjacency_matrix(mc2, mode = "undirected", weighted = TRUE, diag = FALSE)
g_layout = layout.fruchterman.reingold(g_small)
plot.igraph(g_small, vertex.label = NA, layout = g_layout)

#Compute modularity (Q) of the mc2 network given spectral cluster partitioning for 2 to 20 clusters
#The igraph R package includes the function modularity which can be used to compute modularity. Use the graph g_small above.
#Plot Q vs. the number of clusters. How many clusters would you choose based on the results?

Q = unlist(lapply(seq(2,20,1),function(i){
  mc2_specc = specc(as.kernelMatrix(mc2), centers=i)
  v = as.data.frame(factor(mc2_specc))
  rownames(v) = rownames(mc2)
  v = v[names(V(g_small)),]
  return(modularity(g_small,v,weights=E(g_small)$weights))
}))
names(Q) = as.character(seq(2,20,1))

plot(names(Q),Q, type = "l", ylab = "Modularity (Q)", xlab = "# Clusters", xlim = c(1,20))
points(names(Q),Q, pch = 20)
Qmax = which(Q == max(Q))
points(names(Qmax), Q[Qmax], col = "red")
text(as.numeric(names(Qmax)) + 3, Q[Qmax], paste0("Max = ", names(Qmax)," clusters"), col = "red")

