# CBNA_Intermediate_2019_d1

Intermediate Network Biology 2019 Day 1


 Date/Time
Date(s) - 11/09/2019 - 13/09/2019
13:00 - 17:00

Location
440

Categories

    Bio-IT Course
    Bioinformatics
    CBNA
    Course
    Database
    Programming Course
    R
    Template


In modern society, technology has been applied to build and analyze advanced systems in various fields, not only biology as it is most common at EMBL, but also sociology, informatics, engineering and many others. To explore and exploit such systems, we must understand their behavior and internal dynamics.
Graph theory allows us to interpret them as graphs of interconnected components by applying an array of analytical methods and algorithms to uncover a detailed view of the underlying, fundamental properties.

This intermediate 3 half-day course will cover lectures, tutorials, and exercises.
On day one we will look at the basic concepts of graph theory and how to exploit them and evaluate biological networks against random ones to prove that you’re reconstructing meaningful data. Introduction to graph theoretic approaches will show you how to calculate the network-related statistics and evaluate results.
We will also apply algorithms to cluster networks and learn how to benchmark and optimize clustering results across different methods.

On day two and three – introduction to R/iGraph will show you how to build networks from scratch, generate random networks, download and process large scale networks from the public resources such as STRING. You will learn how to filter these networks, annotate them with public data (e.g. Ensembl BioMart) and perform enrichment against databases such as GO, Kegg, and Reactome.
All in the programming environment of R with the option of exploiting the REST API connectivity with Cytoscape.
Day 1

    Graph Theory – An overview of Centralities, Graphlets, and Motifs
    Exploration of networks using centralities such as degree, betweenness, graphlet signature, motifs in search of e.g. dynamic regulatory components such as feed forward loops.
    Network modularity – in search of active subnetworks, communities, and complexes
    Benchmarking clustering results against gold standards and quantifying clustering quality a.k.a. How good are my clusters?

Day 2

    During day 2 you will learn how to use the most common network analysis package for R (iGraph, also available for Python), and basic concepts for biological network data manipulation and enrichment.

    Introduction to network analysis and visualisation using R – Introduction to iGraph
    Data annotation using Ensembl BioMart in R
    Pathway and ontology enrichment analysis using R

Day 3

    Data analysis in iGraph continued.
    Connecting Cytoscape with R and automation

Requirements

This course requires working knowledge of Cytoscape and R. In depth programing experience is not required, you need to understand data structures in R, read, modify and debug R scripts.
Participants will need to bring their own computers with preinstalled software:
Cytoscape v. 3.7.1
R (latest version)
RStudio (latest version)
With installed following packages: iGraph, STRINGdb, plyr, dplyr, reshape, reshape2, clusterProfiler, pathview, biomaRt
Contact

If you have further questions don’t hesitate to contact me at: cbna@embl.de

Matt Rogon
Bookings

Bookings are closed for this event.
