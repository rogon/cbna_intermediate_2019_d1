#!/bin/sh
# Creates plots for properties of models across multiple datasets
# By Jason Lai
USAGE="Usage: $0 -d <dataset> -m <model> -p/-c <parameter/comparison>
       -o <output prefix>"

GNUPLOT="gnuplot"
DIR="`pwd`"

if [ $# -lt 1 ]; then
  echo "$USAGE"
  echo "Additional options: -l         use log scale"
  echo "Examples:"
  echo "  $0 -d bork2455 -d shen-orr -m er -m sf -p clustcoef-avg -o plot"
  exit 1
fi

# Graphcrunch uses relative paths, so it needs to be in the right directory
if [ ! -f "scripts/tools.sh" ]; then
  # assume we're in contrib
  cd "`dirname "$0"`/.."
fi

if [ ! -f "scripts/tools.sh" ]; then
  echo "Please run this script from the graphcrunch directory"
  exit 1
fi

source scripts/tools.sh

nognuplot=0
datasets=""
models=""
cmd=""
cmdarg=""
type=""
filename=""
extra=""
num=0

while getopts d:n:m:p:c:o:l opt; do
  case "$opt" in
    d) dataset="$OPTARG"
       if [ ! -d "data/$dataset" ]; then
         echo "No such dataset. Make sure to run crunch on a graph file first."
         exit 1
       fi
       datasets="`append "$datasets" "$dataset"`"
       ;;
    m) model="$OPTARG"
       scripts/check-params.sh models "$model" || exit 1
       in_list "$model" $models && echo "Duplicate model." && exit 1
       models="`append "$models" "$model"`"
       ;;
    p) parameter="$OPTARG"
       scripts/check-params.sh parameters "$parameter" || exit 1
       if [ -n "$cmd" ]; then echo "Only one parameter/comparison allowed." && exit 1; fi
       cmdarg="$parameter"; cmd="-p"; type="parameters"
       ;;
    c) comparison="$OPTARG"
       scripts/check-params.sh comparisons "$comparison" || exit 1
       if [ -n "$cmd" ]; then echo "Only one parameter/comparison allowed." && exit 1; fi
       cmdarg="$comparison"; cmd="-c"; type="comparisons"
       ;;
    l) extra="`append "$extra" "set log y" "\n"`" ;;
    n) num="$OPTARG" ;;
    o) filename="$OPTARG" ;;
    x) nognuplot=1; exit 1 ;;
    ?) echo "$USAGE"; exit 1 ;;
  esac  
done

mkdir -p "plots"

dataname="`scripts/${type}/${cmdarg} shortname`"
prefix="plots/$filename"

test -z "$filename" && echo "No output filename prefix (-o) provided" && exit 1
test -z "$cmdarg" && echo "No parameter or comparison requested (-p/-c)" && exit 1
test -z "$models" && echo "No models (-m) requested" && exit 1
test -z "$models" && echo "No datasets (-d) requested" && exit 1

for model in $models; do
  printf "" > "${prefix}.data.$model"
done

names=""
num_datasets=0
for dataset in $datasets; do
  num_datasets=`expr $num_datasets + 1`
  names="`append "$names" "'$dataset' $num_datasets" ","`"

  for model in $models; do
    temp="/tmp/temp.plot.tsv.$$"
    ./crunch -d "$dataset" -o "$temp" -n $num -m "$model" $cmd "$cmdarg" || exit 1
    avg=`grep 'AVG' "$temp" | cut -f 4` || exit 1
    stddev=`grep 'STDDEV' "$temp" | cut -f 4` || exit 1
    echo $num_datasets $avg $stddev >> "${prefix}.data.$model"
    rm -f "$temp" || exit 1
  done
done

plots=""
num_models=0
for model in $models; do
  name="`scripts/models/$model shortname`"   # get name
  num_models=`expr ${num_models} + 1`

  params="title \"$name\" with yerrorlines"
  plots="`append "$plots" "'$prefix.data.$model' $params" ","`"
done

echo $names
echo $plots

case "$cmdarg" in
  clustcoef-avg) title="Clustering Coefficients of Networks" ;;
  diameter-avg) title="Average Diameters of Networks" ;;
  graphlet-count) title="Total Number of Graphlets in Networks" ;;
  clust-spectrum\:pearson|clust-spectrum) title="Pearson Correlations of Clustering Spectra" ;;
  #clust-spectrum\:spearman) title="Spearman Correlations of Clustering Spectra" ;;
  degree-distrib\:pearson|degree-distrib) title="Pearson Correlations of Degree Distributions" ;;
  #degree-distrib\:spearman) title="Spearman Correlations of Degree Distributions" ;;
  diameter-spectrum\:pearson|diameter-spectrum) title="Pearson Correlations of Distance Spectra" ;;
  #diameter-spectrum\:spearman) title="Spearman Correlations of Distance Spectra" ;;
  gdd-agreement\:gmean) title="GDD-Agreement (geometric mean) Between the Data and Model Networks" ;;
  gdd-agreement\:amean|gdd-agreement) title="GDD-Agreement (arithmetic mean) Between the Data and Model Networks" ;;
  graphlet-dist) title="RGF-Distance Between the Data and Model Networks" ;;
  clustcoef-avg\:difference) title="Difference of Clustering Coefficients Between the Data and Model Networks" ;;
  clustcoef-avg\:percenterr) title="Percentage Difference of Clustering Coefficients Between the Data and Model Networks" ;;
  diameter-avg\:difference) title="Difference of Average Diameters Between the Data and Model Networks" ;;
  diameter-avg\:percenterr) title="Percentage Difference of Average Diameters Between the Data and Model Networks" ;;
  *) title="${dataname}s of Networks" ;;
esac

case "$cmdarg" in
  clustcoef-avg|diameter-avg|graphlet-count) ylabel="$dataname" ;;
  clust-spectrum|degree-distrib|diameter-spectrum|*\:pearson) ylabel="Pearson Correlation Coefficient" ;;
  #*\:spearman) ylabel="Spearman Correlation Coefficient" ;;
  graphlet-dist) ylabel="RGF-Distance" ;;
  gdd-agreement*) ylabel="GDD-Agreement" ;;
  *\:difference) ylabel="Difference" ;;
  *\:percenterr) ylabel="Percentage Difference" ;;
  *) ylabel="$dataname" ;;
esac

cat > "${prefix}.gnuplot" <<END
set term postscript landscape color solid "Times-Roman" 18 # for inclusion in LaTeX documents.
set output "${prefix}.ps"

set title "$title"
set xlabel "Real-World Networks"
set ylabel "$ylabel"

set xtics ($names)

$extra
plot [0:`expr ${num_models} + 1`] $plots
END

# restore working directory
if [ -n "$DIR" ]; then cd "$DIR"; fi

if [ $nognuplot -eq 0 ]; then
  $GNUPLOT "${prefix}.gnuplot"
fi
