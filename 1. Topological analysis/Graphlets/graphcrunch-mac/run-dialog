#!/usr/bin/perl -w -I./scripts/perl/UI-Dialog-1.08/lib -I./scripts/perl/

use strict;
use warnings FATAL => 'all';
use File::Temp qw/tempfile/;
use File::Basename;
use Text::ParseWords;

use UI::Dialog;
use UI::Dialog::Backend;

# Allow user to set DATADIR
my $DATADIR="data"; $DATADIR = $ENV{'DATADIR'} if $ENV{'DATADIR'};

my $dw = "00";
my $dh = "00";
my $dmh = "00";

sub checklist {
  my $title = shift;
  my $text = shift;
  my @choices = @_;

  my $d = dialog($title);
  my @res = $d->checklist( text => $text, list => \@choices, literal => 1,
                           width => $dw, height => $dh, menuheight => $dmh );
  return undef unless $d->state() eq "OK";
  return \@res;
}

sub escape_sh {
  my $s = shift;
  $s =~ s/(["\\`\$])/\\$1/g; # quote \ ` " $
  return "\"$s\"";
}

my $DIALOGS = ['cdialog'];
my $PATHS = ['/bin', '/usr/bin', '/usr/local/bin/', '/opt/bin/', './bin'];

my $dialog_instance = UI::Dialog->new( debug => 0, backtitle => "GraphCrunch",
                                       order => $DIALOGS, PATH => $PATHS);
sub dialog {
  return $dialog_instance;
}

sub grab_input {
  my $text = shift;
  my $default = shift;

  my $d = dialog;
  my $res = $d->inputbox( text => $text, entry => $default );
  return "" if $d->state ne "OK";
  return $res;
}

sub alert {
  dialog()->msgbox( text => $_[0] );
}

sub intro {
  my $d = dialog("Welcome to GraphCrunch");
  my $text = `cat scripts/text/intro`;
  $text =~ s/\n/\\n/g;
  $d->msgbox( text => $text, literal => 1, width => '00', height => '00');
}

sub pick_data {
  my $ADD = "<------>";
  my $text = "Pick a network to analyze:";
  my @choices = ($ADD, "[Add dataset]", );
  my @sets = glob("$DATADIR/*");
  my $name;
  foreach my $set (@sets) {
    $name = basename $set;
    my $x = `grep '^[0-9][0-9]*\$' "$set/$name/graph.gw"`;
    my @n_e = $x ? split(/\n/, $x) : ('?','?');
    push @choices, $name, $n_e[0] . " nodes, " . $n_e[1] . " edges";
  }

  my $d = dialog;
  my $res = $d->menu( text => $text, list => \@choices, literal => 1,
                      height => $dh, width => $dw, menuheight => $dmh );

  return "" if $d->state() ne "OK";

  if($res eq $ADD) {
    my $filename = select_file("Select a graph file to add:");
    return pick_data() unless $filename;

    my $name = add_dataset($filename);
    return pick_data() unless $name;

    alert("Added '$name' to collection.");
    return $name;
  }

  return $res;
}

sub select_file {
  my $text = shift;
  my $path = shift;
  while(1) {
    my $d = dialog;
    #my $res = $d->inputbox( text => $text, entry => $path );
    my $res = $d->fselect( text => $text, path => $path, width => 0, 
                           height => 0 );
    return "" if $d->state() ne "OK";

    chomp $res; # this shouldn't be necessary
    $res =~ s/\n//g;

    if (! -f$res) {
      alert("No such file '$res'.");
      next;
    } else { return $res; }
  }
}

sub select_file_save {
  my $text = shift;
  my $path = shift;
  while(1) {
    my $d = dialog;
    my $res = $d->inputbox( text => $text, entry => $path );
    return "" if $d->state() ne "OK";
    if (-e$res) {
      $d->yesno(text => "File '$res' already exists. Overwrite?");
      return $res if $d->state eq "OK";
      next;
    } else { return $res; }
  }
}

sub add_dataset {
  my $filename = shift;

  my $name = basename $filename;
  $name =~ s/\..*$//; # delete extension

  if(-e"$DATADIR/$name") {
    my $text = "A data set with that name is already in the collection. " .
               "Use the version in the collection ('no' will quit)?";
    my $d = dialog();
    $d->yesno( text => $text, literal => 1, height => $dh, width=> $dw );
    return $name if $d->state eq "OK";
    return "";
  }

  my ($fh, $temp) = tempfile(UNLINK => 1);

  die if system("scripts/utils/convert2leda > " . escape_sh($temp) . " " .
                                      escape_sh($filename)) > 0;
  die if system("scripts/insert.sh", $temp, $name) > 0;

  return $name;
}

sub pick_num_models {
  while(1) {
    my $res = grab_input("Number of random graphs per model:", 5);
    exit 1 if $res eq "";
    if ($res =~ m/^[0-9]+$/) { return $res; }
    alert "Number out of range: $res";
  }
}

sub pick_models {
  my $dataset = shift;

  my @choices;
  my %models;
  my $mpath;
  my $model;

  my $esc = $dataset;
  $esc =~ s/\ /\\ /g;

  # default all to off if there's any models already created
  my $enable = (glob "$DATADIR/$esc/models/*") ? 0 : 1;

  foreach $mpath (glob "scripts/models/*") {
    chomp (my $menu = `"$mpath" menu`);
    next if $menu eq "";
    foreach my $line ($menu) {
      chomp $line;
      my @words = split(/\t/, $line);

      $models{$words[0]} = [$words[1], $enable && ($words[2] eq "on")];
    }
  }

  foreach $mpath (glob "$DATADIR/$esc/models/*") {
    $model = basename $mpath;
    my ($cmd, @arglist) = split(/:/, $model);
    my $args = join " ", @arglist;
    $models{$model} = [`"scripts/models/$cmd" name $args`, 1];
  }

  foreach $model (sort keys %models) {
    push @choices, $model, $models{$model};
  }

  my $text = "Select a set of random graph models. GraphCrunch will generate " .
             "random graphs corresponding to the real-world " .
             "network according to each of the selected models. " .
             "Use the arrow keys to navigate the menu. " .
             "Press 'space' to select or un-select in this and all subsequent screens.";

  my $res = checklist "Models", $text, @choices;
  exit unless $res;
  return $res;
}

sub pick_parameters {
  my @choices;
  my @parameters = sort glob("scripts/parameters/*");
  foreach my $prop (@parameters) {
    chomp (my $menu = `"$prop" menu`);
    next if $menu eq "";
    foreach my $line (split /\n/, $menu) {
      chomp $line;
      my @words = split(/\t/, $line);
      push @choices, $words[0], [ $words[1], ($words[2] eq "on") ];
    }
  }
  my $text = "Select a set of parameters: ";

  my $res = checklist "parameters", $text, @choices;
  exit unless $res;
  return $res;
}

sub pick_comparisons {
  my @choices;
  my @comparisons = sort glob("scripts/comparisons/*");
  foreach my $prop (@comparisons) {
    chomp (my $menu = `"$prop" menu`);
    next if $menu eq "";
    foreach my $line (split /\n/, $menu) {
      chomp $line;
      my @words = split(/\t/, $line);
      push @choices, $words[0], [ $words[1], ($words[2] eq "on") ];
    }
  }
  my $text = "Select a set of comparisons: ";
  my $res = checklist "Comparisons", $text, @choices;
  exit unless $res;
  return $res;
}

# add remove help OK

sub escape_all {
  my $args = "";

  foreach my $arg (@_) {
    $args = $args . " " . escape_sh($arg);
  }

  return $args;
}

sub tweak {
  my ($models, $props, $comps, $ssh_hosts) = @_;
  my $s_m = join " ", @$models;
  my $s_a = join " ", @$props;
  my $s_c = join " ", @$comps;
  my $s_r = join " ", @$ssh_hosts;

  my $text = "Choose [DONE] to proceed with current selections. " .
             "*Advanced* users may select " .
             "an option and hit OK to modify it. Again, this is for " .
             "EXPERT USERS ONLY!" ;

  my $d = dialog();
  while(1) {
    my @items = ("[DONE]", "Run crunch", "models", $s_m,
                 "parameters", $s_a, "comparisons", $s_c, "SSH hosts", $s_r);

    my $res = $d->menu( text => $text, literal => 1, list => \@items,
                        width => $dw, height => $dh, menuheight => $dmh );

    exit 1 if $d->state ne "OK";
    if($res eq "models") {
      $res =$d->inputbox( text => "Change models to:", entry => $s_m, literal => 1 );
      next if $d->state ne "OK";
      $s_m = $res;
    } elsif($res eq "parameters") {
      $res = $d->inputbox( text => "Change parameters to:", entry => $s_a, literal => 1 );
      next if $d->state ne "OK";
      $s_a = $res;
    } elsif($res eq "comparisons") {
      $res = $d->inputbox( text => "Change comparisons to:", entry => $s_c, literal => 1 );
      next if $d->state ne "OK";
      $s_c = $res;
    } elsif($res eq "SSH hosts") {
      my $sshdesc = "If you have access to additional machines on a cluster " .
          "sharing NFS home directories, and already have passwordless SSH " .
          "logins set up, GraphCrunch can distribute the workload to the " .
          "other machines if you list them here:";
      $res = $d->inputbox( text=> $sshdesc, entry => $s_r, literal => 1 );
      next if $d->state ne "OK";
      $s_r = $res;
    } elsif($res eq "[DONE]") {
      last;
    }
  }

  @$models = split " ", $s_m;
  @$props = split " ", $s_a;
  @$comps = split " ", $s_c;
  @$ssh_hosts = split " ", $s_r;
}

sub main {
  my $name;

  if($ARGV[0]) {
    if( -e$ARGV[0] ) {
      intro();
      $name = add_dataset($ARGV[0]);
    } elsif( -d("$DATADIR/".$ARGV[0]) ) {
      $name = $ARGV[0];
    } else {
      print STDERR "No such file '" . $ARGV[0] . "'.\n";
      exit 1;
    }
  } else {
    intro();
    $name = pick_data;
  }
  exit 1 if $name eq "";

  my $models = pick_models($name);
  my $num_models = pick_num_models();
  my $props = pick_parameters();
  my $comps = pick_comparisons();
  my $outfile = select_file_save("Save results to...", "$name.out.tsv");
  my $ssh_hosts = [];
  tweak($models, $props, $comps, $ssh_hosts);

  system("./crunch", "-d", $name, "-o", $outfile,
         "-m", join(" ", @$models), "-n", $num_models,
         "-p", join(" ", @$props), "-c", join(" ", @$comps),
         "-r", join(" ", @$ssh_hosts));
}

main();

