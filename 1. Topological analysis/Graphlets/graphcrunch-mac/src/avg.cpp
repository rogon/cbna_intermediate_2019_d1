#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

int main()
{
    double total = 0;
    long count = 0;

    while(cin.good())
    {
        double value;
        cin >> value;

        if(cin.good()) // valid
        {
            total += value;
            count++;
        }
    }

    if(count == 0)
    {
        cout << endl;
    }
    else
    {
        double avg = total / (double)count;
        cout << avg << endl;
    }
}
