/* Adds a list of numbers */

#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

int main()
{
    long long total = 0;

    while(cin.good())
    {
        long long num;
        cin >> num;
        if(cin.eof()) break;
        else if(cin.fail()) { cerr << "ERROR ADDING"; exit(1); }
        else total += num;
    }

    cout << total << endl;
}
