/* Agreement between two frequency distributions for graphlet node classes
 * S(k) = degree(k) / k
 * N(k) = S(k) / sum S(k)
 * D(G,H) = (sum ([N(k) for G] - [N(k) for H])^2)^0.5
 * agreement = 1 - D(G,H)
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <map>

int main(int argc, char *argv[])
{
    if(argc < 3)
    {
        fprintf(stderr, "Usage: %d <file1> <file2>\n", argv[0]); return 
1;
    }
    FILE *fp = fopen(argv[1], "r");
    if(!fp) { perror(argv[1]); return 1; }

    std::map<long long, int> values;
    std::map<long long, double> values1;
    std::map<long long, double> values2;

    double total1 = 0;
    while(!feof(fp))
    {
        long long a, b;
        if( fscanf(fp, "%lld %lld", &a, &b) == 2)
        {
            if(a == 0) continue;
            values[a] = 1;
            values1[a] = (double) (b) / (double) (a);
            total1 += values1[a];
        }
        else fscanf(fp, "%*s\n");
    }

    fclose(fp);

    fp = fopen(argv[2], "r");
    if(!fp) { perror(argv[2]); return 1; }

    double total2 = 0;
    while(!feof(fp))
    {
        long long a, b;
        if( fscanf(fp, "%lld %lld", &a, &b) == 2)
        {
            if(a == 0) continue;
            values[a] = 1;
            values2[a] = (double) (b) / (double) (a);
            total2 += values2[a];
        }
        else fscanf(fp, "%*s\n");
    }

    fclose(fp);

    double total = 0;
    total1 = total1 ? total1 : 1;
    total2 = total2 ? total2 : 1;

    std::map<long long, int>::iterator iter;
    for(iter = values.begin(); iter != values.end(); ++iter)
    {
        long long i = iter->first;

        double x = values1[i]/total1 - values2[i]/total2;
        total += x*x;
    }

    printf("%.6lf\n", (1 - sqrt(total)));

    return 0;
}
