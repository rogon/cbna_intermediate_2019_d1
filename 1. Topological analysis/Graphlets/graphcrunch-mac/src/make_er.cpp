/*****************************************************************************
+ Generates a Erdos-Renyi random graph
+ Usage: make_er <# nodes> <# edges> <output graph>
*****************************************************************************/

#include <LEDA/graph/graph_iterator.h>
#include <LEDA/system/stream.h>
#include <LEDA/graph/graph_alg.h>
#include <LEDA/core/dictionary.h>
#include <LEDA/core/random_source.h>

#include <unistd.h>
#include <math.h>
#include <time.h>

#include <stdarg.h>
#include <assert.h>

#if 1 || defined(LEDA_STD_HEADERS)
using namespace leda;
#endif

char *OUTFILE_NAME = "random_graph.gw";

int
main (int argc, char *argv[])
{
    /* Use C-type I/O just for error checking, since I'm a bozo and
     * don't know how to check for file existence in C++.
     */
    GRAPH < short, short > G;
    int n,m;

    if(argc != 3)
    {
        fprintf(stderr, "Usage: %s <nodes> <medges>\n", argv[0]);
        return 1;
    }

    rand_int.set_seed(time(NULL) + getpid());


    n=atoi(argv[1]); // number of nodes
    m=atoi(argv[2]); // number of edges
    random_graph(G,n,m,true,true,true); //creates simple undirected graph G(n,m)
    node v;
    int i=0;
    forall_nodes(v,G)
      {
        i++;
        G[v]=i;
      }
    //G.write("/dev/fd/1");
    G.write();
    //cout << "written to " << OUTFILE_NAME << '\n';
    return 0;
}


