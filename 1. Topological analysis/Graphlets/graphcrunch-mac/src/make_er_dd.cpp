/*******************************************************************************
+ Read in graph and construct a random graph with the same degree distribution.
+ Usage: make_er_dd <input graph> <output graph>
*******************************************************************************/

#include <LEDA/graph/graph_iterator.h>
#include <LEDA/system/stream.h>
#include <LEDA/graph/graph_alg.h>
#include <LEDA/graph/node_map2.h>
#include <LEDA/core/dictionary.h>
#include <LEDA/core/array.h>

#include <math.h>
#include <time.h>
#include <unistd.h>

#include <stdarg.h>
#include <assert.h>

#if 1 || defined(LEDA_STD_HEADERS)
using namespace leda;
#endif

static FILE *tty;

char *OUTFILE_NAME = "make_gw1.gw";

void Fatal(const char *fmt, ...)
{
    va_list ap;
    fflush(stdout);
    fprintf(stderr, "Fatal Error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    if(!isatty(fileno(stderr)))
    {
        if(!tty)
            tty = fopen("/dev/tty", "w");
        fprintf(tty, "Fatal Error: ");
        va_start(ap, fmt);
        vfprintf(tty, fmt, ap);
        va_end(ap);
        fprintf(tty, "\n");
    }
    assert(false);
    exit(1);
}


void RndGraphDistrib (char infile_name[])
{
    // read in graph G
    GRAPH < string, short > G;
    GRAPH < short, short > G_rand;
    if(G.read(infile_name) != 0)
      {
        Fatal("Error in reading the graph file %s.",
                (const char*)(infile_name));
      }

    int node_num=G.number_of_nodes();
    node V_G[node_num];

    node n1;
    int i = 0;
    forall_nodes(n1,G)
      {
	V_G[i]=n1;
	++i;
      }

    int n=G.number_of_nodes(); // so n = node_num.  Oh, well...
    //int m=G.number_of_edges();
    node* V = new node [n]; // array V of new graph's nodes

    int rand_number1;
    int num_iter=0, num_experiments=0;

    while (num_experiments < 1000)
      {
	num_experiments++;
//	cout << "experiment " << num_experiments << "\n";
	G_rand.clear();

	//int* deg = new int[n];
	array<int> deg(n);

	for (i=0; i<n; i++) 
	  { 
	    V[i]=G_rand.new_node();
	    deg[i]=G.degree(V_G[i]);
	    G_rand[V[i]]=i+1;
	  }

	deg.sort();
    
	srand48(time(NULL) + getpid()); //seed the rand num generator with time and process id

	// declare node_map2 to keep track if there are edges between two nodes (don't care about direction)
	node_map2<bool> C(G_rand,true);

	for (i=n-1; i>-1; i--)
	  {
	    //cout << "for loop i = " << i << ", deg = " << deg[i] << "\n";
	    num_iter=0;
	    //node v=V[i];
	    int d = deg[i];
	    while (d > 0)
	      {
		if (num_iter > 3*n) break;
		num_iter++;

		rand_number1=(int)(drand48()*node_num); //rand_number1 is a uniform random int between 0 and node_num-1
		//cout << i << " " << d << " rand1 = " << rand_number1 << "\n";
		if (i==rand_number1) continue;
		//cout << "not self\n";
		if (!C(V[rand_number1],V[i]) || !C(V[i],V[rand_number1])) continue;
		//cout << "not already\n";
		if (deg[rand_number1] <= 0) continue;
		//cout << "deg ok\n";
		G_rand.new_edge(V[i],V[rand_number1]);
		C(V[rand_number1],V[i]) = false;
		C(V[i],V[rand_number1]) = false;
		deg[rand_number1]--;
		deg[i]--;
		d--;
		//cout << "while: d = " << d << "\n";
	      } // end of while (d>0)
	    if (d != 0) break;
	  } // end of for-loop
	if (i==-1) break;
      } //end of while (num_experiments < 1000)
//    cout << "Number of experiments = " << num_experiments << ".\n";
    //G_rand.write("/dev/fd/1"); // write to standard output
    G_rand.write();
}


int main (int argc, char *argv[])
{
    /* Use C-type I/O just for error checking, since I'm a bozo and 
     * don't know how to check for file existence in C++.
     */
    FILE *graph_fp;
    //FILE *out_fp;

/* Error checking for input and output files. C I/O */
    if(!argv[1])
        Fatal("USAGE: make_rand_graph_dd_Linux input-graph-file");

    if((graph_fp=fopen(argv[1], "r"))==NULL)
        Fatal("Can't open graph file %s.", argv[1]);
//    if((out_fp=fopen(argv[2], "r"))!=NULL)
//        Fatal("File %s already exists.  Give a name of a non-existing file.", argv[2]);
    fclose(graph_fp);
/* If we get here, bi_dir_fp doesn't exist & graph_ft does. */
/* End of error checking */

/* C++ type I/O */ 
    /*    ifstream graph_stream(argv[1]); */

    //ofstream out_stream(argv[2]);

//    int n,m;
//    n=atoi(argv[1]); // number of nodes
//    m=atoi(argv[2]); // number of edges
	
    RndGraphDistrib(argv[1]);

    /* Closing the input and output files. */
    /*    graph_stream.close();*/
//    out_stream.close();

    return 0;
}
