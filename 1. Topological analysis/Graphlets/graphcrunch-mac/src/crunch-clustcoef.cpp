/****************************************************************************
+ Outputs average clustering coefficient by degree
+ Usage: crunch-clustcoef <input graph> <output file name>
+ Outputs: to the output file: a list of (degree [tab] frequency)
+          to standard out: the average clustering coefficient
*****************************************************************************/

#include <LEDA/graph/graph_iterator.h>
#include <LEDA/system/stream.h>
#include <LEDA/graphics/graphwin.h>
#include <LEDA/graph/graph_alg.h>
#include <LEDA/graphics/panel.h>
#include <LEDA/core/dictionary.h>
#include <LEDA/core/array.h>

#include <math.h>
#include <time.h>
#include <unistd.h>

#include <stdarg.h>
#include <assert.h>

#if defined(LEDA_STD_HEADERS)
//using namespace std;
#endif
using namespace leda;
using std::ofstream;
using std::cin;
using std::cout;

static FILE *tty;

void Fatal(const char *fmt, ...)
{
    va_list ap;
    fflush(stdout);
    fprintf(stderr, "Fatal Error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    if(!isatty(fileno(stderr)))
    {
        if(!tty)
            tty = fopen("/dev/tty", "w");
        fprintf(tty, "Fatal Error: ");
        va_start(ap, fmt);
        vfprintf(tty, fmt, ap);
        va_end(ap);
        fprintf(tty, "\n");
    }
    assert(false);
    exit(1);
}

void CopyGraph(GRAPH<node,edge>& H, const graph& G, const list<node>& V)
{ // constructs the subgraph of G induced by V.
  H.clear();
  node_array<node> v_in_H(G,nil);
  node v;
  forall(v,V) v_in_H[v] = H.new_node(v);
  forall(v,V)
  { node u = v_in_H[v];
    edge e;
    forall_adj_edges(e,v)
    {
      if(v_in_H[target(e)] != nil)
          H.new_edge(u,v_in_H[target(e)],e);
    }
  }
}

double choose(int n, int k)
{
  double ch=1.0;
  int i;
  for (i=0; i <= k-1; ++i)
    {
      double v1=n-i;
      double v2=k-i;
      ch = ch * v1/v2;
    }
  return ch;
}

void ClustCoeff (char infile_name[], ofstream &out_stream)
{
    // read in graph G
    GRAPH < string, short > G;
    if(G.read(infile_name) != 0)
      {
        Fatal("Error in reading the graph file %s.",
                (const char*)(infile_name));
      }

    //    int n=G.number_of_nodes();
    //    int m=G.number_of_edges();

    int max=-1; // max degree
    node v;
    forall_nodes(v,G)
      if(G.degree(v)>max) max=G.degree(v);

    double C_total = 0.0;

    double C_v[max+1]; // C_v[i] is sum of all C_v for all nodes v of degree i
    double Deg_v[max+1];  // Deg_v[i] is the number of nodes of degree i

    int i;
    for(i=0;i<=max;i++)
      {
	C_v[i]=0.0;
	Deg_v[i]=0.0;
      }

    G.make_bidirected();
    node v1;

    forall_nodes(v,G)
      {
	GRAPH <node,edge> Neighbors;
	list<node> subnodes;
	subnodes.clear();
	forall_adj_nodes(v1,v)
	  {
	    subnodes.append(v1);
	  }
        CopyGraph(Neighbors, G, subnodes);// TADA!!!!!
	int N_num_nodes=Neighbors.number_of_nodes();
	int N_num_edges=Neighbors.number_of_edges(); // twice the edges since bidirected
	double tmp;
	if (N_num_nodes > 1)
	   tmp = double(N_num_edges) / double(N_num_nodes*(N_num_nodes-1));
	else tmp = 0.0;

        C_total = C_total + tmp;

	C_v[G.degree(v)/2] = C_v[G.degree(v)/2] + tmp;
	Deg_v[G.degree(v)/2] = Deg_v[G.degree(v)/2] + 1.0;
      }

    for (i=0; i<=max;i++)
      if (Deg_v[i] > 0) // if the number of nodes of degree i is > 0
	out_stream << i << " " << C_v[i]/Deg_v[i] << "\n"; // printing degree k and clust. coeff C(k)
    //G_rand.write("/dev/fd/1"); // write to standard output

    std::cout << C_total / G.number_of_nodes() << std::endl;
}


int main (int argc, char *argv[])
{
    /* Use C-type I/O just for error checking, since I'm a bozo and 
     * don't know how to check for file existence in C++.
     */
    FILE *graph_fp;
    //FILE *out_fp;

/* Error checking for input and output files. C I/O */
    if(!argv[1] || !argv[2])
        Fatal("USAGE: crunch-clustcoef input-graph-file output-C(k)-distrib-file");

    if((graph_fp=fopen(argv[1], "r"))==NULL)
        Fatal("Can't open graph file %s.", argv[1]);
    fclose(graph_fp);

    //if((out_fp=fopen(argv[2], "r"))!=NULL)
    //    Fatal("File %s already exists.  Give a name of a non-existing file.", argv[2]);

    /* C++ type I/O */ 
    /* ifstream graph_stream(argv[1]); */

    ofstream out_stream(argv[2]);

//    int n,m;
//    n=atoi(argv[1]); // number of nodes
//    m=atoi(argv[2]); // number of edges
	
    ClustCoeff(argv[1], out_stream);

    /* Closing the input and output files. */
    /*    graph_stream.close();*/
    out_stream.close();

    return 0;
}
