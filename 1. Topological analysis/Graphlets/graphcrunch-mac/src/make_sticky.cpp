/*******************************************************************************
+ Read in two files: one contains a list of nodes, the other edges, and then
+ write out the graph in GW format.
*******************************************************************************/

#include <LEDA/graph/graph_iterator.h>
#include <LEDA/system/stream.h>
#include <LEDA/graph/graph_alg.h>
#include <LEDA/core/dictionary.h>

#include <math.h>
#include <time.h>

#include <stdarg.h>
#include <assert.h>

#include <iostream>
#include <fstream>
#include <string.h>
#include <strings.h>

#if defined(LEDA_STD_HEADERS)
//using namespace std;
#endif
// This is for Leda 4.4.1 evaluation
using namespace leda;
using std::ofstream;
using std::cin;
using std::cout;
// end of inserts for Leda 4.4.1 evaluation
//#include <LEDA/CopyGraph.c> // this is for UMD genome machines

/* for rusage */
#include <unistd.h>


static FILE *tty;

char *INFILE_NAME = "make_gw.gw";
/* char *OUTFILE_NAME = "bidirectional_edges.tex"; */

void Fatal(const char *fmt, ...)
{
    va_list ap;
    fflush(stdout);
    fprintf(stderr, "Fatal Error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    if(!isatty(fileno(stderr)))
    {
        if(!tty)
            tty = fopen("/dev/tty", "w");
        fprintf(tty, "Fatal Error: ");
        va_start(ap, fmt);
        vfprintf(tty, fmt, ap);
        va_end(ap);
        fprintf(tty, "\n");
    }
    assert(false);
    exit(1);
}

void CopyGraph(GRAPH<node,edge>& H, const graph& G, const list<node>& V)
{ // constructs the subgraph of G induced by V.
  H.clear();
  node_array<node> v_in_H(G,nil);
  node v;
  forall(v,V) v_in_H[v] = H.new_node(v);
  forall(v,V)
  { node u = v_in_H[v];
    edge e;
    forall_adj_edges(e,v)
    {
      if(v_in_H[target(e)] != nil)
          H.new_edge(u,v_in_H[target(e)],e);
    }
  }
}

void CreateStickyGraph (char *infile_name, char *outfile_name)
{
    GRAPH < string, short > G;

    if(G.read(infile_name) != 0)
      {
	Fatal("Error in reading the graph file %s.",
		(const char*)(infile_name));
      }

    node_array<int> Deg(G,0); // Declare array of nodes Deg and initialize it to 0.
    node n;
    edge e;
    int num = G.number_of_nodes();
    double sum = 0;
    double sum_squares = 0;
    double avg, stdev;

    srand48(time(NULL) + getpid()); //seed the rand num generator with time and process id

    forall_nodes(n,G)
    {
      Deg[n] = G.degree(n);
      sum = sum + Deg[n];
      sum_squares = sum_squares + Deg[n]*Deg[n];
    }
    avg = sum/num;
    stdev = sqrt((sum_squares - sum*sum/num)/(num-1));

    /* Print out Fan-Out into file. */
    G.sort_nodes(Deg);

    // Compute Theta_i:
    double sqrt_sum_deg=sqrt(sum);
    int i=0;
    double Theta_array[num];
    forall_nodes(n,G)
    {
       Theta_array[i] = Deg[n]/sqrt_sum_deg;;
      i++;
    }

    // Construct a graph H of sticky-model type:
    GRAPH <int, int> H;
    // create nodes of H:
    node V[num];
    for (i=0;i<num;i++)
      {
	V[i]=H.new_node(); H[V[i]]=i;
      }
    //cout << "Nodes of H created.\n"; // debug
    // create edges of H
    int j;
    for (i=0;i<num;i++)
      for (j=i;j<num;j++)
	{
	  //cout << "H[V[i]]=" << H[V[i]] << "; H[V[j]]=" << H[V[j]] << ".\t";
	  double RND=drand48(); // choses random number in [0,1)
	  //cout << "RND=" << RND << "\n";
	  if (Theta_array[i]*Theta_array[j]>=RND)
	    {
	      //cout << "New edge to be created.\n";
	      e=H.new_edge(V[i],V[j]); // edge between nodes i and j created with prob.
	      H[e]=1; // just a convetional weight of 1 on every edge 
	      //cout << "New edge created.\n--------------\n";
	    }
	}
    
    //out_stream << "|V(H)| = " << H.number_of_nodes() << "; |E(H)| = " << H.number_of_edges() << ".\n";
    //out_stream << "Theta(i):\n";
    for (i=0;i<num;i++)
      {
	//out_stream << Theta_array[i] << "\t";
	div_t d=div(i,10);
	//if (d.rem == 0) out_stream << "\n";
      }
    //cout << "Theta written to output.\n";
    //out_stream << "\n--------------------\n";

    //out_stream << "#Gene\tIn-degree\tOut-degree\tDegree\n";
    //out_stream << "#Degree\n";
    //out_stream << "#-------------\n";

    // degree distrib of graph H belonging to the sticky-model:
    
    // For some reason node_array<int> DegH(H,0) doesn't work at  DegH[n] = H.degree(n); BUG.
    //node_array<int> DegH(H,0); // Declare array of nodes Deg and initialize it to 0.
    //forall_nodes(n,H)
    // {
    //   cout << "Inside of forall_nodes(n,H).\n";
    //   cout << "H.degree(n) = " << H.degree(n) << ".\n";
    //   DegH[n] = H.degree(n);
    //   sum = sum + Deg[n];
    //   sum_squares = sum_squares + DegH[n]*DegH[n];
    // }
    //cout << "Finished computing degrees of H.\n";
    //avg = sum/num;
    //stdev = sqrt((sum_squares - sum*sum/num)/(num-1));

    /* Print out Fan-Out into file. */
    //H.sort_nodes(DegH);
    //cout << "Finished sorting nodes of H by degree.\n";
    
    //out_stream << "Degree distrib of H:\n";
    /*
    forall_nodes(n,H)
      {
	out_stream << H.degree(n) << "\n";
      }*/
    //out_stream << "#------\n";
    //out_stream << "#avg.deg.\t\t\t" << avg << "\n";
    //out_stream << "#st.dev.\t\t\t" << stdev << "\n";

    // Write out the new graph H:
    H.write(outfile_name); // writen to file
}


int main (int argc, char *argv[])
{
    /* Use C-type I/O just for error checking, since I'm a bozo and 
     * don't know how to check for file existence in C++.
     */
    FILE *graph_fp;

/* Error checking for input and output files. C I/O */
    if(!argv[1] || !argv[2])
        Fatal("USAGE: sticky_model input-graph-file output-graph-file");

    if((graph_fp=fopen(argv[1], "r"))==NULL)
        Fatal("Can't open graph file %s.", argv[1]);
    fclose(graph_fp);

/* C++ type I/O */ 
    /*    ifstream graph_stream(argv[1]); */

    CreateStickyGraph(argv[1], argv[2]);

    return 0;
}
