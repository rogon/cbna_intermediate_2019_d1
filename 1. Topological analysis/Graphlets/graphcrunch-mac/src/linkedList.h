/* support class needed by make_sf.cpp */

typedef struct List
{ 
	long Data;
	List* Next;
	List(){
		Next = NULL;
		Data = 0;
	}
};
typedef List* ListPtr;

class SLList
{
 private:
 public:
	SLList();
	~SLList();
	ListPtr SLList::Previous(long index);
	ListPtr SLList::Previous(ListPtr index);
	void SLList::AddANode();
	void SLList::Advance();
	void SLList::Rewind();
	void SLList::DeleteANode(ListPtr corpse);
	void SLList::PrintList();
	void SLList::RewindToHead();
	void SLList::InsertANodeAfter(ListPtr point);

	ListPtr Head, Tail, CurrentPtr;
	
};
