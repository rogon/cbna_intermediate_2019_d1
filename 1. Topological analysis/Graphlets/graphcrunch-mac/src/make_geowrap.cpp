/* Creates a geometric random graph
 * Usage: make_geowrap <# nodes> <# edges>
 * Extra options: -o <output file>        otherwise, outputs to stdout
 *                -w                      allow edge across left/right or
 *                                        top/bottom "sides" of the square
 *                -s <degree sequence>    merge with a sticky index
 *                                        based on the provided deg sequence
 * How it works:
 *   Randomly scatters points in a unit square. Creates edges between all
 *   nodes within a maximum distance of each other; the maximum distance
 *   is chosen in this generator to meet the desired number of edges.
 *
 *   With wrapping mode on, edges can cross the boundaries of the unit
 *   square to wrap to the other side; for example, a node near the
 *   left side and a node near the right side would be considered close
 *   under this model, rather that relatively far apart.
 *
 *   With sticky indices, an edge is created only when the nodes are close
 *   enough together AND a random number exceeds the threshhold required by
 *   the stickiness index for that node. Stickiness indices are arbitrarily
 *   assigned to nodes, although the indices themselves are based on the
 *   degree sequence of the input graph.
 *
 *   See make_sticky for more on the stickiness model.
 *
 *   Points are scattered uniformly (pseudo-)randomly by default. There is
 *   also an untested, undocumented Gaussian distribution feature.
 *   Use at your own risk.
 *
 *    - July 2006, Jason Lai
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <queue>
#include <vector>
#include <time.h>
#include <numeric>
#include <unistd.h>

using namespace std;

typedef struct
{
    double distance;
    int i;
    int j;
} edge;

// STL is just ... weird
bool operator< (const edge &e1, const edge &e2)
{
    return e1.distance > e2.distance;
}

/* Based on the Python gauss implementation by Lambert Meertens */
double gauss(double mu, double sigma)
{
    return mu + sigma * cos(2*M_PI*drand48()) * sqrt(-2 * log(1.0-drand48()));
}

bool generate_geo(int V, int E, int dim = 3, bool wrap = true,
                  ostream &out = cout, double gauss_sigma = 0,
                  vector<int> *degs = NULL)
{
    srand48( time(NULL) + getpid() );

    /* GCC extension; non-ISO compliant */
    /* double points[V][dim]; */

    /* Ok, fine, let's do it the standards-compliant way */
    double **points;
    points = new double *[V];
    for(int x = 0; x < V; x++)
        points[x] = new double[dim];

    /* priority queue of edges */
    priority_queue< edge, vector<edge> > *edge_queue =
        new priority_queue< edge, vector<edge> > ( less<edge>() );

    double deg_ss = 0, sticky1 = 0, sticky2 = 0;
    if(degs) deg_ss = sqrt((double)accumulate(degs->begin(), degs->end(), 0));

    int i, j, d;
    /* randomly place points in a unit n-cube */
    for(i = 0; i < V; i++)
    {
        for(d = 0; d < dim; d++)
        {
            if(gauss_sigma > 0)
                points[i][d] = gauss(0.5, gauss_sigma);
            else
                points[i][d] = drand48();
        }
    }

    /* for each pair of points */
    double *p1, *p2;
    for(i = 0; i < V; i++)
    {
        p1 = points[i];
        if(degs) sticky1 = (*degs)[i]/deg_ss;
        for(j = i + 1; j < V; j++) /* each pair examined only once */
        {
            p2 = points[j];
            if(degs) /* use geosticky model */
            {
                sticky2 = (*degs)[j]/deg_ss;
                if(drand48() > sticky1 * sticky2) continue;
            }

            double ss = 0; // sum of squares
            if(wrap)
            {
                for(d = 0; d < dim; d++)
                {
                    double diff = fabs( p2[d] - p1[d] );
                    if(diff > 0.5) /* shorter path by wrapping around */
                        diff = 1.0 - diff;

                    ss += diff * diff;
                }
            }
            else
            {
                for(d = 0; d < dim; d++)
                    ss += (p2[d] - p1[d]) * (p2[d] - p1[d]);
            }

            double distance = sqrt(ss);

            edge e = {distance, i, j};
            edge_queue->push(e);

            /* we could save memory here by only remembering the E*1.01 edges 
               with the lowest distance, but memory is cheap */
        }
    }

    /* get the E lowest distance edges */
    int num_edges = 0;
    vector<edge> edges;
    while( !edge_queue->empty() )
    {
        if(num_edges >= E) break;

        edge e = edge_queue->top();
        num_edges++;

        edges.push_back(e);
        edge_queue->pop();
    }

    if(num_edges < E * 0.99 || num_edges > E * 1.01)
    {
        cerr << "Could not create requested edges within threshold." << endl;
        cerr << "Created " << num_edges << "/" << E << endl;
        return false;
    }

    out << "LEDA.GRAPH" << endl;
    out << "string" << endl << "short" << endl;
    out << V << endl;
    for(i = 0; i < V; i++)
        out << "|{" << i+1 << "}|" << endl;

    out << edges.size() << endl;
    vector<edge>::iterator it;
    for(it = edges.begin(); it != edges.end(); ++it)
        out << it->i + 1 << " " << it->j + 1 << " 0 |{}|" << endl;

    delete edge_queue;

    return true;
}

void usage()
{
    cerr << "Usage: make_geowrap <# nodes> <# edges>" << endl << endl;
    cerr << "Options: " << endl;
    cerr << "    -d <dimension>    default: 3 dimensions" << endl;
    cerr << "    -w                enable boundary wrapping" << endl;
    cerr << "    -o <filename>     write to filename; default: stdout" << endl;
    cerr << "    -s <filename>     degree sequence for sticky indices" << endl;
    cerr << endl;
}

vector<int> *read_degree_sequence(istream &in)
{
    vector<int> *degs = new vector<int>();
    while( in.good() )
    {
        int deg;
        in >> deg;
        if(!in.good()) break;
        degs->push_back(deg);
    }
    return degs;
}

int main(int argc, char **argv)
{
    int dim = 3;
    bool wrap = false;
    ostream *out = &cout;
    ofstream file;
    double gauss_sigma = 0;
    vector<int> *degs = NULL;
    ifstream deg_file;

    int val;
    while( (val = getopt(argc, argv, "s:wxg:o:d:")) >= 0 )
    {
        switch(val)
        {
            case 'w': wrap = true; break;
            case 'd': dim = atoi(optarg); break;
            case 'g': gauss_sigma = strtod(optarg, NULL); break;
            case 's': deg_file.open(optarg);
                      degs = read_degree_sequence(deg_file);
                      deg_file.close();
                      break;
            case 'o': file.open(optarg, ofstream::out);
                      out = &file; break;
            case '?': usage(); exit(1);
        }
    }

    if(argc - optind != 2)
    {
        usage();
        exit(2);
    }

    int V = atoi(argv[optind]);
    int E = atoi(argv[optind+1]);

    if(V <= 0)
    {
        cerr << "Must have more than 0 nodes and edges." << endl;
        exit(2);
    }

    if(degs && (int) degs->size() != V)
    {
        cerr << "Degree sequence must match desired number of nodes." << endl;
        exit(2);
    }

    bool result = generate_geo(V, E, dim, wrap, *out, gauss_sigma, degs);
    if( file.is_open() ) file.close();

    if(!result) return 1;
    return 0;
}
