/* Prints a list of integers
 * Usage: integers [start] <end> [step]
 * Steps over [step] integers each time
 * If step is negative, start should be >= end
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

int main(int argc, char **argv)
{
    int width, i, step = 1, a = 0, b = 0;

    if(argc < 2)
    {
        fprintf(stderr, "Usage: %s [start] <end> [step]", argv[0]);
        return 1;
    }

    width = strlen(argv[1]);
    if(argc > 2) { a = atoi(argv[1]); } else { b = atoi(argv[1]); }
    if(argc > 2) b = atoi(argv[2]);
    if(argc > 3) step = atoi(argv[3]);

    if(step == 0) { assert(step != 0); exit(1); }
    else if(step > 0)
    {
        for(i = a; i <= b; i += step)
            printf("%0*d\n", width, i);
    }
    else if(step < 0)
    {
        for(i = a; i >= b; i += step)
            printf("%0*d\n", width, i);
    }

     return 0;
}

