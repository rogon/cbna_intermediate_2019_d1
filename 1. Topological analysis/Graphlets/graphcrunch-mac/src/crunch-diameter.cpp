/*****************************************************************************
+ Finds average diameter from average shortest path length between nodes
+ (within each connected subgraph)
+ Usage: crunch-diamater <input graph> <output file name>
+ Outputs: to the output file: a list of (length [tab] frequency)
+          to standard out: the average shortest path length
*****************************************************************************/

#include <LEDA/graph/graph_iterator.h>
#include <LEDA/graph/templates/shortest_path.h>
#include <LEDA/system/stream.h>
#include <LEDA/graph/graph_alg.h>
#include <LEDA/core/d_array.h>

#include <math.h>
#include <time.h>

#include <stdarg.h>
#include <assert.h>
#include <string.h>

#include <iostream>
#include <fstream>

#if 1 || defined(LEDA_STD_HEADERS)
using namespace leda;
#endif

void Fatal(const char *fmt, ...)
{
    va_list ap;
    fflush(stdout);
    fprintf(stderr, "Fatal Error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    exit(1);
}


void TestShPaths (char infile_name[], char outfile_name[])
{
    GRAPH < string, short > G;
//    edge e;
    node n1, n2;

    if(G.read(infile_name) != 0)
      {
	Fatal("Error in reading the graph file %s.",
		(const char*)(infile_name));
      }
    edge_array<int> cost(G,1); /* declaring edge array c of G and initializing it to all 1s. */
    int G_n = G.number_of_nodes();
    node_matrix<int> DIST(G,G_n + 1);  /* declaring a distance matrix for G and initialising it to |V|+1. */

    G.make_undirected();

  //  out_stream << "The matrix of all pairs shortest paths: \n\n\n";
 
    if (!ALL_PAIRS_SHORTEST_PATHS_T<int>(G,cost,DIST))
      Fatal("The graph contains negative cycle(s).");

    long long avg_diam = 0;
    long num = 0;
    d_array<long, long long> freq;

    /* Print output into file. */
    forall_nodes(n1,G)
      forall_nodes(n2,G)
      {
        int dist = DIST(n1,n2);
	if (!(dist <= 0 || dist >= G_n + 1))
	  { 
	    avg_diam = avg_diam + DIST(n1,n2);
	    num++;
            freq[dist]++;
	  }
      }

    printf("%lf\n", (double)avg_diam/num);

    std::ofstream out_stream(outfile_name);

    int x;
    forall_defined(x, freq)
    {
        out_stream << x << "\t" << freq[x] << "\n";
    }
    out_stream.close();
}


int main (int argc, char *argv[])
{
    /* Use C-type I/O just for error checking, since I'm a bozo and 
     * don't know how to check for file existence in C++.
     */
    FILE *graph_fp;

/* Error checking for input and output files. C I/O */
    if(argc < 3)
        Fatal("USAGE: crunch-diameter input-graph-file out-file");

    if((graph_fp=fopen(argv[1], "r"))==NULL)
        Fatal("Can't open graph file %s.", argv[1]);

    //if((out_fp=fopen(argv[2], "r"))!=NULL && strcmp(argv[2], "/dev/null"))
    //    Fatal("File %s already exists.  Give a name of a non-existing file.", argv[2]);
    fclose(graph_fp);

/* If we get here, bi_dir_fp doesn't exist & graph_ft does. */
/* End of error checking */

/* C++ type I/O */ 
    /*    ifstream graph_stream(argv[1]); */

    TestShPaths(argv[1], argv[2]);

    return 0;
}
