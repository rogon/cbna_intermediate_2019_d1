/* Sample standard deviation (n-1) on a list of numbers separated by newlines
 */

#include <iostream>
#include <list>
#include <stdlib.h>
#include <math.h>

using namespace std;

int main()
{
    double total = 0;
    double sum = 0;
    list<double> data;

    while(cin.good())
    {
        double value;

        cin >> value;
        if(cin.good())
        {
            data.push_back(value);
            total += value;
        }
        else
        {
            cin.clear();
            break;
        }
    }

    double avg = total / data.size();

    list<double>::iterator it;
    for(it = data.begin(); it != data.end(); ++it)
    {
        sum += (*it - avg) * (*it - avg);
    }

    if(data.size() > 1)
        cout << sqrt(sum/(data.size()-1.0)) << endl;
    else
        cout << "nan" << endl;

    return 0;
}
