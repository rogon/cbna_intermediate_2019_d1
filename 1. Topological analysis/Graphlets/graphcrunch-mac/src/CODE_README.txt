You're on your own here.

Listing/summary:
add.cpp                  sum of numbers
avg.cpp                  avg of numbers
class-agree.cpp          used in graphlet degree distrib agreement
corr.cpp                 calcs spearman and pearson correlation coeffients
crunch-clustcoef.cpp     calcs clustering coefficient and spectra
crunch-diameter.cpp      calcs average shortest path lengths and spectra
integers                 prints a list of integers in a given range
linkedList.cpp           helper for make_sf
linkedList.h             helper for make_sf
make_er.cpp              Erdos-Renyi graph generator
make_er_dd.cpp           Erdos-Renyi following a degree distribution
make_geowrap.cpp         Geometric random graph generator
make_sf.cpp              Barabasi-Albert scale-free graph generator
make_sticky.cpp          Stickiness index generator
ncount.cpp               graphlet counting
stddev.cpp               standard deviation (n-1)

The following require Algorithmic Solution's LEDA graph library to compile:
make_er.cpp
make_er_dd.cpp
make_sticky.cpp
crunch-clustcoef.cpp
crunch-diameter.cpp

We have only tested this with LEDA 5.0.1. Compiling with 4.x will need
modifications (particularly header locations) to work. Earlier versions
will likely need major changes to compile. Make sure to change the 
LEDA_DIR path in the Makefile.

Got a compilation error about an "undefined reference to std::basic_string"?
Make sure your version of GCC/G++ matches your version of LEDA.
