/* Make a Barabasi-Albert scale free graph
 * Usage: make_sf -n <nodes> -m <edges per turn> -o <output name> -w
 * Note: using the -o and -w flags will create two output files,
 *       including one LEDA graph with the .gw extension.
 *
 * Edges per turn should be edges/nodes from the original graph so that
 * the same number of edges are achieved. This can be a decimal.
 */

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include "linkedList.h"

//edited to support probablistic non-integer degrees.

long * dupeFlag;
long binCount, binSize, graphNum;
SLList* AdjList;
long* bin;
long addDegree;
float fAddDegree;
long order;
long edges;

void printUsageError(){
	fprintf(stderr,"Usage: sfgen [-n order]\n"
	               "             [-m added edges per turn] \n"
		       "             [-o ouput filename] \n"
		       "             [-2 (for mcl output)]\n"
		       "             [-w (for leda gw output)]\n"
	       );
}

//*****************************************************************************
/* clearBin *******************************************************************
******************************************************************************/
void clearBin()
{
	long i;
	for(i=0; i<binSize ; i++){
		bin[i]=0;
	}
	binCount=0;
}



//*****************************************************************************
/* clearAdjMat ****************************************************************
******************************************************************************/

void clearAdjList()
{
	long i;

	for(i=0; i<order; i++){
		while(AdjList[i].Head != AdjList[i].Tail){
			AdjList[i].DeleteANode(AdjList[i].Tail);
		}
		AdjList[i].Head->Data = i;
	}

}

//*****************************************************************************
/* outputGraph ****************************************************************
******************************************************************************/
void outputGraph(char *filename)
{
	printf("Writing to file \"%s\"...\n",filename);
	long i;

	FILE *fstream;

	if((fstream = fopen(filename,"w"))){//opening file stream

		for(i=0; i< order; i++){
			//			AdjList[i].PrintList();

			AdjList[i].AddANode();
			AdjList[i].RewindToHead();
			while(AdjList[i].CurrentPtr->Next != NULL){
				fprintf(fstream,"%d ",(int) AdjList[i].CurrentPtr->Data);
				AdjList[i].Advance();
			}
			fprintf(fstream,"-1\n");
			AdjList[i].DeleteANode(AdjList[i].Tail);

		}
		fclose(fstream);
	} else {
		printf("FILE OPEN ERROR\n");
		exit(0);
	}
	return;
}

//*****************************************************************************
/* outputGraphMCL *************************************************************
******************************************************************************/
void outputGraphMCL(char *filename)
{
	printf("Writing to file \"%s\"...\n",filename);
	long i;

	FILE *fstream;

	if((fstream = fopen(filename,"w"))){//opening file stream

		fprintf(fstream,"(mclheader\nmcltype matrix\ndimensions %dx%d\n)\n(mclmatrix\nbegin\n",
				  (int) order, (int) order);

		for(i=0; i< order; i++){
			//			AdjList[i].PrintList();
			
			AdjList[i].AddANode();
			AdjList[i].RewindToHead();
			while(AdjList[i].CurrentPtr->Next != NULL){
				fprintf(fstream,"%d ",(int) AdjList[i].CurrentPtr->Data);
				AdjList[i].Advance();
			}
			fprintf(fstream,"$\n");
			AdjList[i].DeleteANode(AdjList[i].Tail);
		}
		fprintf(fstream,")\n");
		
		fclose(fstream);
	} else {
		printf("FILE OPEN ERROR\n");
		exit(0);
	}
	return;
}

//*****************************************************************************
/* outputGraphLeda ************************************************************
******************************************************************************/

void outputGraphLeda(char *filename)
{
	int i;
	FILE *fstream = fopen(filename, "w");

	if(!fstream) {
		fprintf(stderr, "Error opening path %s", filename);
		exit(1);
	}

        fprintf(fstream, "LEDA.GRAPH\nstring\nshort\n");
	fprintf(fstream, "%d\n", order);

	// print nodes
	for(i = 0; i < order; i++)
             fprintf(fstream, "|{%d}|\n", i+1);

	// count total number of edges
	int num_edges = 0;
	for(i = 0; i < order; i++) {
		AdjList[i].AddANode();
		AdjList[i].RewindToHead();
		AdjList[i].Advance(); // skip first
		while(AdjList[i].CurrentPtr->Next != NULL) {
			num_edges++;
			AdjList[i].Advance();
		}
		AdjList[i].DeleteANode(AdjList[i].Tail);
	}

	fprintf(fstream, "%d\n", num_edges);

	// print edges
	for(i = 0; i < order; i++) {
		AdjList[i].AddANode();
		AdjList[i].RewindToHead();
		AdjList[i].Advance(); // skip first
		while(AdjList[i].CurrentPtr->Next != NULL) {
			fprintf(fstream,"%d %d 0 |{1}|\n", i+1, (int) AdjList[i].CurrentPtr->Data+1);
			AdjList[i].Advance();
		}
		AdjList[i].DeleteANode(AdjList[i].Tail);
	}
	fclose(fstream);
}

//*****************************************************************************
/* fillList *******************************************************************
******************************************************************************/
void fillList()
{
   int deg;

	printf("Filling...\n");
	long i, j, k, newedge, index;
	int flag;
	
	for(i=0; i<addDegree; i++){
		bin[binCount]=i;
		binCount++;
	}


	for(i=addDegree; i<order; i++){
		//add a vertex

		deg = (int)(fAddDegree+(float)rand()/RAND_MAX);
		if(i==addDegree) deg = addDegree;

		//  		printf("degree %d\n",deg); 
		edges += deg;
		for(j=0; j<deg; j++){
			//add an edge
			//DOESN'T CHECK FOR DUPLICATES.
			index = rand() % (binCount);
			newedge = bin[index];

			flag = 1;
			for(k=0; k<j; k++){
				if(newedge == dupeFlag[k]){
					flag = 0;
				}
			}
			
			if(flag){
				AdjList[newedge].AddANode();
				AdjList[newedge].Tail->Data = i;
				dupeFlag[j] = newedge;
			}else{//the edge is already there.
				j--;
				continue;
			}
				
		}
		
		for(j=0; j<addDegree; j++){
			bin[binCount]=i;
			binCount++;
			bin[binCount]=dupeFlag[j];
			binCount++;
			dupeFlag[j]=-1;
		}

	}

	return;
}


//*****************************************************************************
/* printMatrix ****************************************************************
******************************************************************************/
void printMatrix()
{
	long i, j;
	for(i=0; i<order; i++){
		AdjList[i].RewindToHead();
		AdjList[i].Advance();
		
		for(j=0; j<order; j++){
			if(AdjList[i].CurrentPtr->Data == j){
				printf(" *");
				AdjList[i].Advance();
			} else {
				printf(" .");
			}			
		}
		printf("\n");


	}
}



//*****************************************************************************
/* main ***********************************************************************
******************************************************************************/
int main(int argc, char **argv)
{
	
	long c;
	bool gwout = false;
	bool mclout = false;
	edges = 0;
        char *filename;

	binCount = 0;
	order = 0; addDegree = 0; filename = "graph.gra";
   while ((c = getopt(argc, argv, "n:m:o:2w")) != EOF) {
      switch (c) {
			
      case 'm': //initial coclique size (# of edges added per turn.)
			addDegree = atol(optarg);
			fAddDegree = atof(optarg);
			break;
			
      case 'n': //order.
			order = atol(optarg);
			break;
			
      case 'o': //define filename.
			filename = optarg;
			break;
			
      case '2': //used if we want mcl output as well.
			mclout = true;
			break;

      case 'w': // for LEDA's gw output as well
			gwout = true;
			break;

      case '?': //invalid command line format
			printUsageError();
			return 1;
			break;
			
      default: //invalid command line format
			printUsageError();
			return 1;
			break;
			
      }//END SWITCH
	}//END WHILE c      
  
	if( (!(order && addDegree)) || (order <= addDegree) ){
		printUsageError();
		return 0;
	}

	printf("Allocating memory...\n");
	binSize = addDegree*2*(order-addDegree)+addDegree;
	bin = new long[binSize];
	AdjList = new SLList[order];

	//	AdjMat = new bool[CONST_N][CONST_N];
	dupeFlag = new long[addDegree];

	// Seed random number generator.
	srand( time(NULL) + getpid() );

	clearAdjList();
	
	clearBin();
	
	fillList();
	

	outputGraph(filename);

	char filename2 [ strlen(filename) + 10 ];

	if(mclout){
		sprintf(filename2, "%s.m", filename);
		outputGraphMCL(filename2);
	}

	if(gwout){
		sprintf(filename2, "%s.gw", filename);
		outputGraphLeda(filename2);
	}

	//		printMatrix();

	printf("%d vertices, %d edges\n",(int)order, (int)edges);

	return 0;
}
