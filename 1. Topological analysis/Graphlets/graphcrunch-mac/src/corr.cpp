/* Calculate Correlation Coefficients
 * Usage: corr <-p|-s> <distribution 1> <distribution 2>
 *
 * Works using either Pearson's Rank Correlation or
 * Spearman's Non-Parametric Rank Correlation (default)
 *
 * Fills missing values with zero. For the data that GraphCrunch provides,
 * this is probably the best method, although results can be very different
 * using other methods, so consider the ramifications.
 *
 * Ranks for Spearman are computed using midranks, giving tied values the
 * average of the ranks normally. For example, given 2, 5, 5, 8, 11, 42
 * the ranks are 1, 2.5, 2.5, 4, 5, 6
 *
 * Spearman is calculated as Pearson(numbers by ranks)
 */

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <cmath>

using namespace std;
typedef map<long, double> Freq;
typedef map<double, long> Freq_rev;

Freq &read(const char *filename)
{
    long a;
    double b;

    Freq *values = new Freq;

    ifstream in(filename);

    if(in.fail()) { perror(filename); exit(1); }

    while(in.good())
    {
        in >> a;
        in >> b;
        (*values)[a] = b;
    }

    in.close();

    return *values;
}

Freq &ranks(Freq &old)
{
    Freq *ranks = new Freq;
    typedef vector< pair<double, long> > Freq_inv;
    Freq_inv inv;

    for(Freq::iterator it = old.begin(); it != old.end(); ++it)
        inv.push_back( make_pair<double, long>(it->second, it->first) );

    sort( inv.begin(), inv.end() );

    double last_value = -LONG_MAX;
    int tied_count_sum = 0;
    long last_index = 0;
    vector<long> ties;

    ranks->clear();

    int count = 0;
    for(Freq_inv::iterator it = inv.begin(); it != inv.end(); ++it)
    {
        ++count;

        if(it->first != last_value) // new value
        {
            (*ranks)[it->second] = count;

            if(!ties.empty()) // flush the ties out
            {
                double rank = double(tied_count_sum) / ties.size();
                for(vector<long>::iterator t = ties.begin(); t != ties.end(); ++t)
                    (*ranks)[*t] = rank;
                ties.clear();
            }

            last_value = it->first;
            tied_count_sum = count;
            last_index = it->second;
        }
        else /* tie */
        {
            if(ties.empty()) ties.push_back(last_index);
            ties.push_back(it->second);
            tied_count_sum += count;
        }
    }

    if(!ties.empty()) // flush the ties out
    {
        double rank = double(tied_count_sum) / ties.size();
        for(vector<long>::iterator t = ties.begin(); t != ties.end(); ++t)
            (*ranks)[*t] = rank;
        ties.clear();
    }

    return *ranks;
}

void zero_missing(Freq &values1, Freq &values2)
{
    set<long> keys;

    for(Freq::const_iterator it = values1.begin(); it != values1.end(); ++it)
        keys.insert( it->first );

    for(Freq::const_iterator it = values2.begin(); it != values2.end(); ++it)
        keys.insert( it->first );

    for(set<long>::iterator it = keys.begin(); it != keys.end(); ++it)
    {
        if(values1.find(*it) == values1.end()) values1[*it] = 0;
        if(values2.find(*it) == values2.end()) values2[*it] = 0;
    }
}

/*
def pearson(x, y):
    x_avg = sum(x.itervalues()) / float(len(x))
    y_avg = sum(y.itervalues()) / float(len(y))
    x_y_sum  = sum([(x[a] - x_avg) * (y[b] - y_avg) for a, b in zip(x,y)])
    x_sq_sum = sum([(z - x_avg)**2 for z in x.itervalues()])
    y_sq_sum = sum([(z - y_avg)**2 for z in y.itervalues()])
    return float(x_y_sum) / math.sqrt(x_sq_sum * y_sq_sum)
*/

double pearson(Freq &values1, Freq &values2)
{
    double x_sum = 0, y_sum = 0;
    for(Freq::iterator it = values1.begin(); it != values1.end(); ++it)
        x_sum += it->second;
    for(Freq::iterator it = values2.begin(); it != values2.end(); ++it)
        y_sum += it->second;

    double x_avg = x_sum / values1.size();
    double y_avg = y_sum / values2.size();

    double x_sq_sum = 0;
    double y_sq_sum = 0;
    double x_y_sum = 0;

    for(Freq::iterator it = values1.begin(); it != values1.end(); ++it)
    {
        long key = it->first;
        double x_diff_avg = values1[key] - x_avg;
        double y_diff_avg = values2[key] - y_avg;

        x_sq_sum += x_diff_avg * x_diff_avg;
        y_sq_sum += y_diff_avg * y_diff_avg;

        x_y_sum  += x_diff_avg * y_diff_avg;
    }

    return x_y_sum / sqrt(x_sq_sum * y_sq_sum);
}

void usage(const char *name)
{
    cerr << "Usage: " << name << " [--pearson|--spearman] <freq1> <freq2>" << endl;
    cerr << "       (default pearson)" << endl;
}

int main(int argc, char **argv)
{
    bool spearman = false;
    int optoff = 1;

    if(argc < 2)
    {
        usage(argv[0]);
        return 1;
    }

    if(!strcmp(argv[optoff], "--spearman"))
    {
        spearman = true;
        optoff++;
    }
    if(!strcmp(argv[optoff], "--pearson"))
    {
        spearman = false;
        optoff++;
    }

    if(argc - optoff < 2)
    {
        usage(argv[0]);
        return 1;
    }

    char *file1 = argv[optoff];
    char *file2 = argv[optoff+1];

    Freq &values1 = read(file1);
    Freq &values2 = read(file2);
    zero_missing(values1, values2);

    if(spearman)
    {
        Freq &ranks1 = ranks(values1);
        Freq &ranks2 = ranks(values2);
        cout << pearson(ranks1, ranks2) << endl;
    }
    else
    {
        cout << pearson(values1, values2) << endl;
    }

    return 0;
}
