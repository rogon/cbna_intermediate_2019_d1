#!/bin/sh

. ${GCDIR-.}/scripts/tools.sh

type="$1"
shift

name=""
case $type in
  models) name="model" ;;
  parameters) name="parameter" ;;
  comparisons) name="comparison" ;;
  *) name="(unknown)" ;;
esac

error=0
# for each
for full in "$@"; do
  split="`split_char : $full`"
  item="`car $split`"
  args="`cdr $split`"

  script="${GCDIR-.}/scripts/${type}/${item}"
  if [ ! -f "$script" ]; then
    echo "Not a valid $name: ${item}" 1>&2
    error=1
    continue
  fi

  # error on spearman argument - added 9/28/2007 - naveen
  if [ "$name" = "comparison" -a "$args" = "spearman" ]; then
    echo "Not a valid $name: $full" 1>&2
    error=1
    continue
  fi

done
exit $error
