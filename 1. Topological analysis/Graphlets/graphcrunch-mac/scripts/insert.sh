#!/bin/sh
# Setup a graph for processing
USAGE="Usage: $0 <input graph> <graph name>"

. ${GCDIR-.}/scripts/tools.sh

if [ $# -ne 2 ]; then warn "$USAGE"; exit 1; fi

graph="$1"
name="$2"

if [ ! -d "$DATADIR/$name" ]; then
  mkdir -p "$DATADIR/$name/" || exit 1
  mkdir -p "$DATADIR/$name/$name/" || exit 1
  mkdir -p "$DATADIR/$name/models/" || exit 1
  cp "$graph" "$DATADIR/$name/$name/graph.gw" || exit 1
  echo "$DATADIR/$name/$name/graph.gw" > "${GCDIR-.}/batch/queue/$name"
fi
