#!/bin/sh
# Basic template for comparing two frequency distributions
# The name of the comparison must match the name of the parameter

. ${GCDIR-.}/scripts/tools.sh
. ${GCDIR-.}/scripts/run-comparison.sh

DEFAULT="pearson"
ATTR="`basename "$0"`"

get () {
  export PATH="$BIN:$PATH"
  compare "${3-$DEFAULT}" "$ATTR" "$1" "$2" || exit 1
}

name () {
  mode_name "${1-$DEFAULT}"
}

shortname () {
  short_mode_name "${1-$DEFAULT}"
}

mode_name () {
  case "$1" in
    pearson) echo "$NAME via Pearson's Correlation Coefficient";;
    spearman) echo "$NAME via Spearman's Rank Correlation Coefficient";;
    *) echo "$NAME"
  esac
}

short_mode_name () {
  case "$1" in
    pearson) echo "${SHORTNAME-$NAME} (Pearson)";;
    spearman) echo "${SHORTNAME-$NAME} (Spearman)";;
    *) echo "${SHORTNAME-$NAME}"
  esac
}

menu () {
  menu_item "`basename "$0"`:pearson" "$NAME (Pearson)" "on"
  # removed per requst by Tijana - 9/27/07 - naveen
  #menu_item "`basename "$0"`:spearman" "$NAME (Spearman)" "off"
}

compare () {
  # example: compare pearson diameter-freq model1/ model2/
  FILE1=`${GCDIR-.}/scripts/parameters/"$2" getfile "$3"`
  FILE2=`${GCDIR-.}/scripts/parameters/"$2" getfile "$4"`
  "$BIN/$1" "$FILE1" "$FILE2"
}

