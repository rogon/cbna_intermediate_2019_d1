#!/bin/sh

# Delete all data
# Hopefully this won't be necessary when this is done ;)

if [ "$1" != "confirm" ]; then
  echo "Usage: $0 confirm" >&2
  echo "NOTICE: this will delete all existing data!" >&2
  exit 1
fi

# check if right directory
if [ ! -f ${GCDIR-.}/scripts/tools.sh ]; then echo "Wrong dir"; exit 1; fi

rm -Rf ${GCDIR-.}/data/*
rm -f ${GCDIR-.}/batch/queue/*
rm -f ${GCDIR-.}/batch/fail/*
rm -f ${GCDIR-.}/batch/busy/*
rm -f ${GCDIR-.}/batch/locks/*
rm -f tmp/*
