#!/bin/sh
# Compares an original network to its random models
USAGE="Usage: $0 <graph name> <models> <# models> <props> <comps>"

. ${GCDIR-.}/scripts/tools.sh

if [ $# -lt 5 ]; then warn "$USAGE"; exit 1; fi

name="$1"
orig="$DATADIR/$1/$1/"
models="$2"
num_inst="$3"
props="$4"
comps="$5"

debug_info "Calling orig-vs.sh on $1"
xdata="$name"`printf '\t'`"DATA"`printf '\t'`""
count=2
for prop in $props; do
  split=`split_char : $prop`
  prop=`car $split`
  args=`cdr $split`

  debug_info "Orig-vs.sh prop $prop"
  data="`${GCDIR-.}/scripts/parameters/$prop get "$orig" $args`" || die
  xdata="$xdata"`printf '\t'`"$data"
  span="`${GCDIR-.}/scripts/parameters/$prop span`"
  count=`expr $count + $span`
done

for comp in $comps; do
  split=`split_char : $comp`
  comp=`car $split`
  args=`cdr $split`

  debug_info "Orig-vs.sh comp $comp"
  span="`${GCDIR-.}/scripts/comparisons/$comp span $args`" || die
  for i in `"$BIN/integers" $span`; do
    xdata="$xdata"`printf '\t'`""
  done
  count=`expr $count + $span`
done

echo "$xdata"

MTMP="`temp_file orig-vs`"
trap "rm -f \"$MTMP\"" 0

# do for models
for model_full in $models; do
  split=`split_char : $model_full`
  model=`car $split`
  args=`cdr $split`

  instbase="$DATADIR/${name}/models/${model_full}"
  max=$num_inst

  echo > "$MTMP"
  for i in `"$BIN/integers" 1 $max`; do
    inst="$instbase/$name-${model_full}-${i}/"
    ${GCDIR-.}/scripts/orig-vs-model.sh "$orig" "$inst" "$props" "$comps" | tee -a "$MTMP"
  done
  ${GCDIR-.}/scripts/quickstats.sh $count "$MTMP" "$name" "$model"
  rm -f "$MTMP"
done
