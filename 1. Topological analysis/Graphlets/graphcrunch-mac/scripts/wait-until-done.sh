#!/bin/sh

DATADIR="${DATADIR-"${GCDIR-.}/data"}"

if [ $# -lt 1 ]; then echo "Specify dir"; exit 1; fi

for m in "`ls "$DATADIR/$1/"`"; do
  if [ ! -d "$m" ]; then continue; fi
  mdir="$dir/$m"

  for i in "`ls "$mdir"`"; do
    idir="$mdir/$i"
    until [ -e "$idir/done" ]; do
      sleep 1
    done
  done
done
