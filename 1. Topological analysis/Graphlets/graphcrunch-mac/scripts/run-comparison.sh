#!/bin/sh

. ${GCDIR-.}/scripts/run-base.sh
. ${GCDIR-.}/scripts/prop-helper.sh

run_comparison () {
  if [ $# -lt 1 ]; then echo "$0: not enough arguments"; exit 1; fi

  run_base "$@"; status=$?
  if [ $status -ne 118 ]; then exit $status; fi

  option="$1"
  shift
  case "$option" in
    span) span "$@"; return ;;
    get)  get "$@"; return ;;

    *) return 118 ;;
  esac
}

run () {
  run_comparison $@; exit $?
}
