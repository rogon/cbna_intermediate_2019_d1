#!/bin/sh

. ${GCDIR-.}/scripts/run-base.sh

run_model () {
  if [ $# -lt 1 ]; then echo "$0: not enough arguments"; exit 1; fi

  run_base "$@"; status=$?
  if [ $status -ne 118 ]; then exit $status; fi

  option="$1"
  shift
  case "$option" in
    gen|generate)
        if [ $# -lt 2 ]; then
          echo "Usage: $0 <input graph> <output graph>"
          exit 2
        fi

        generate "$@"; return ;;

    *) return 118 ;;
  esac
}

run () {
  run_model "$@"; exit $?
}
