#!/bin/sh
USAGE="$0 <numcols> <file> <name> <model>"

BIN="${GCDIR-.}/bin"

if [ $# -ne 4 ]; then echo "$USAGE"; exit 1; fi

cols=`expr $1 + 1`
file="$2"
name="$3"
model="$4"
avg="${name}`printf '\t'`"$model"`printf '\t'`AVG"
for i in `"$BIN/integers" 4 $cols`; do
  avg="$avg`printf '\t'``cut -f $i "$file" | $BIN/avg`"
done
echo "$avg"
dev="${name}`printf '\t'`"$model"`printf '\t'`STDDEV"
for i in `"$BIN/integers" 4 $cols`; do
  dev="$dev`printf '\t'``cut -f $i "$file" | $BIN/stddev`"
done
echo "$dev"
