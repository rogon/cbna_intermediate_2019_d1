#!/bin/sh

case "$1" in
  models) ;;
  parameters) ;;
  comparisons) ;;
  *) echo "Unknown script type!" >&2; exit 1
esac

for item in `ls ${GCDIR-.}/scripts/"$1"/`; do
  "${GCDIR-.}/scripts/$1/$item" menu
done | cut -f1,2
