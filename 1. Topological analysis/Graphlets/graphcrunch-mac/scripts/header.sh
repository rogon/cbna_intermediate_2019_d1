#!/bin/sh

if [ $# -ne 2 ]; then exit 1; fi

. ${GCDIR-.}/scripts/tools.sh

props="$1"
comps="$2"

xdata="Data Network"`printf '\t'`"Model Networks"`printf '\t'`"Random Networks/Stats"
for prop in $props; do
  split=`split_char : $prop`
  prop=`car $split`
  args=`cdr $split`

  data="`${GCDIR-.}/scripts/parameters/"$prop" shortname $args`" || die "Error in parameter $prop"
  xdata="$xdata"`printf '\t'`"$data"
done

for comp in $comps; do
  split=`split_char : $comp`
  comp=`car $split`
  args=`cdr $split`

  data=`${GCDIR-.}/scripts/comparisons/"$comp" shortname $args` || die "Error in comparison $comp"
  xdata="$xdata"`printf '\t'`"$data"
done
echo "$xdata"

