#!/bin/sh
# Generic functions for parameters and comparisons

BIN="${GCDIR-.}/bin"

warn () {
  echo "$@" >&2
}

debug_info () {
  if [ -n "$DEBUG" ]; then echo "DEBUG:" "$@" >&2; fi
}

name () {
  echo ${NAME-`basename $0`}
}

desc () {
  citation "$@"
}

public () {
  test ${PRIVATE-no} != "yes"; return $?
}

# helper function
menu_item () {
  text="$1"
  while shift; do
    text="$text`printf '\t'`$1"
  done
  echo "$text"
}

menu () {
  public || return  # no menu entries for private parameters
  menu_item "`basename $0`" "`name`" "on"
}

citation () {
  echo "${CITATION-}"
}

checkargs () {
  # return zero if arguments are valid, non-zero otherwise
  return 0
}

shortname () {
  echo "${SHORTNAME-"`name "$@"`"}"
}

depend () {
  echo ${DEPEND-}
}

run_base () {
  option="$1"
  shift
  case "$option" in
    name) name "$@"; return ;;
    shortname) shortname "$@"; return ;;
    public) public "$@"; return $?;;
    menu) menu "$@"; return ;;
    desc) desc "$@"; return ;;
    citation) citation "$@"; return ;;
    checkargs) checkargs "$@"; return ;;

    *) return 118
  esac
}


# nasty hack!
trap 'run "$@"' 0
