#!/bin/sh
# Generic functions for parameters and comparisons

_DEFAULT=0

# At least one of these should be overridden
get () {
  if [ "$_DEFAULT" -eq "$$" ]; then warn "getfile not implemented"; return; fi
  cat "`_DEFAULT="$$" getfile "$@"`" || exit 1
}

# Should only be for parameters, but I'm lazy.
# Comparisons don't handle "getfile" anyway.
getfile () {
  if [ "$_DEFAULT" -eq "$$" ]; then warn "get not implemented"; return; fi

  file="$1._data.`basename "$0"`""`join_char ":" "$@"`"
  if [ "$CACHE" != "no" -a -s "$file" ]; then   # memoization, sort of
    echo "$file"
    return
  else
    _DEFAULT="$$" get "$@" > "$file"
    echo "$file"
  fi
}

span () {
  # number of columns; not fully implemented
  echo ${SPAN-1}
}

