#!/bin/sh

#PATH="${GCDIR-.}/scripts/utils:bin:$PATH"
DATADIR="${DATADIR-"${GCDIR-.}/data"}"
BIN="${GCDIR-.}/bin"

# helper utiltites

debug_info () {
  if [ -n "$DEBUG" ]; then echo "DEBUG:" "$@" >&2; fi
}

temp_file () {
  if which mktemp > /dev/null 2> /dev/null; then
    mktemp "/tmp/graphcrunch-${1-gc}-XXXXXX"
  else # use our own tmp dir, avoids race conditions
    printf "${GCDIR-.}tmp/graphcrunch-%s.%s.%d" "${1-gc}" "`hostname`" $$
  fi
}

split_char () {
  IFS="$1"
  echo $2 | tr -d '\n\r'
}

join_char () {
  text="$1"
  shift
  for arg in "$@"; do
    text="$text$1$arg"
  done
  printf "%s" "$text"
}

append () {
  if [ -n "$1" ]; then
    printf "%s%s%s" "$1" "${3- }" "$2"
  else
    printf "%s" "$2"
  fi
}

car () {
  printf "%s" $1
}

cdr () {
  shift
  echo $@ | tr -d '\n\r'
}

parse_nodes_edges() {
  # set variables nodes and edges
  nodes_edges="`grep '^[0-9][0-9]*[^ 0-9]*$' "$1"`" || exit 1
  nodes=`car $nodes_edges`
  edges=`cdr $nodes_edges`
}

warn () {
  echo "`basename "$0"`: $@" >&2
}

die () {
  echo "`basename "$0"`: $@" >&2
  exit 1
}

info () {
  echo "$@" >&2
}

in_list () {
  needle="$1"
  shift;
  for i in $@; do
    if [ "$i" = "$needle" ]; then return 0; fi
  done

  return 1
}
