#!/bin/sh
# Generates random graphs and inserts them into the queue for processing

USAGE="Usage: $0 <input graph> <graph name> <# instances> [model...]"
# Example usage: generate mygraph.gw mygraph 5 er sf
# If the # of instances is less than 1, all existing instances will be used

. ${GCDIR-.}/scripts/tools.sh

if [ $# -lt 3 ]; then echo "$USAGE"; exit 1; fi

graph="$1"
base="$2"
max="$3"

# check that $max is a number
if echo "$max" | grep -v "^[0-9][0-9]*$" > /dev/null; then
  warn "$USAGE"
  exit 1
fi

shift; shift; shift

# for each model
for model_full in "$@"; do
  split=`split_char : $model_full`
  model=`car $split`
  args=`cdr $split`

  script="${GCDIR-.}/scripts/models/${model}"
  if [ ! -f "$script" ]; then
    warn "No script to generate random model type ${model}!"
    continue
  fi

  skip=0
  echo "Generating $max $model model instances"
  for i in `"$BIN/integers" 1 $max`; do
    fname="${base}-${model_full}-${i}"
    dir="$DATADIR/$base/models/${model_full}/$fname"
    name="$dir/graph.gw"

    # Don't overwrite existing graphs
    if [ -f "$name" ]; then
      skip=`expr $skip + 1`
      continue
    fi

    temp="`temp_file generate-models`"

    # Ask the model to generate a graph
    "$script" generate "$graph" "$temp" $args || die "Error generating $name"

    # Copy new random graph to data directory
    mkdir -p "$dir" || exit 1
    mv -f "$temp" "$name" || exit 1

    # Add to queue
    echo "$name" > "${GCDIR-.}/batch/queue/$fname"
  done
  if [ $skip -gt 0 ]; then
    echo "(Skipped $skip instances already present)"
  fi
done
