#!/bin/sh
# Basic template for comparing two numbers
# The name of the comparison must match the name of the property

. ${GCDIR-.}/scripts/tools.sh
. ${GCDIR-.}/scripts/run-comparison.sh

DEFAULT="difference"

get () {
  export PATH="$BIN:$PATH"
  compare "${3-$DEFAULT}" `basename "$0"` "$1" "$2" || exit 1
}

name () {
  mode_name "${1-$DEFAULT}"
}

shortname () {
  short_mode_name "${1-$DEFAULT}"
}

mode_name () {
  case "$1" in
    difference) echo "$NAME |difference|";;
    percenterr) echo "$NAME % error";;
  esac
}

short_mode_name () {
  case "$1" in
    difference) echo "${SHORTNAME-$NAME} |diff|";;
    percenterr) echo "${SHORTNAME-$NAME} % err";;
  esac
}

_t="`printf '\t'`"

menu () {
  menu_item "`basename "$0"`:difference" "$NAME |difference|" "on"
  menu_item "`basename "$0"`:percenterr" "$NAME % error" "off"
}

difference () {
  echo "$1" "$2" |
      awk '{if($1 > $2) print ($1-$2); else print ($2-$1)};'
}

percenterr () {
  echo "$1" "$2" |
  awk '{ if($1 > 0) { print 100 * (($1 > $2) ? ($1 - $2) : ($2 - $1) ) / $1 }; }'
}

compare () {
  # example: compare difference diameter-avg model1/ model2/
  p1=`${GCDIR-.}/scripts/parameters/"$2" get "$3"`
  p2=`${GCDIR-.}/scripts/parameters/"$2" get "$4"`
  case "$1" in
    difference) difference $p1 $p2 ;;
    percenterr) percenterr $p1 $p2 ;;
    *) "$BIN/$1" "$p1" "$p2";;
  esac
}

