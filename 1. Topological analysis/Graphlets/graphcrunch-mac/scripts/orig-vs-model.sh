#!/bin/sh
# Compares a single random graph to the original network
USAGE="Usage: $0 <graph1> <graph2> <props> <comps>"

. ${GCDIR-.}/scripts/tools.sh

if [ $# -lt 4 ]; then warn "$USAGE"; exit 1; fi

graph1="$1"
graph2="$2"
props="$3"
comps="$4"

dir1="`dirname "$graph2"`"
dir2="`dirname "$dir1"`"
dir3="`dirname "$dir2"`"

unpacked=""
if [ -f "$graph2/_unpack.tgz" ]; then
  debug_info "Unpacking $graph2 data"
  unpacked="`tar -ztf "$graph2/_unpack.tgz"`"
  tar -zx -f "$graph2/_unpack.tgz" -C "$graph2"
fi

status=0

num="`basename "$graph2" | sed -e 's/^.*-\([0-9][0-9]*\)$/\1/'`"
xdata="`basename "$dir3"`"`printf '\t'`"`basename "$dir1"`"`printf '\t'`"$num"

debug_info "$0 `basename $1` vs `basename $2`"

for prop in $props; do
  split=`split_char : $prop`
  prop=`car $split`
  args=`cdr $split`

  debug_info "$0 prop $prop"
  data="`${GCDIR-.}/scripts/parameters/"$prop" get "$graph2"`"
  if [ $? -ne 0 ]; then status=1; fi
  xdata="$xdata"`printf '\t'`"$data"
done

for comp in $comps; do
  split=`split_char : $comp`
  comp=`car $split`
  args=`cdr $split`

  debug_info "$0 comp $comp"
  data="`${GCDIR-.}/scripts/comparisons/"$comp" get "$graph1" "$graph2" $args`"
  if [ $? -ne 0 ]; then status=1; fi
  xdata="$xdata"`printf '\t'`"$data"
done
echo "$xdata"

if [ -n "$unpacked" ]; then
  for f in $unpacked; do
    rm -f "$graph2/$f"
  done
fi

exit $status
