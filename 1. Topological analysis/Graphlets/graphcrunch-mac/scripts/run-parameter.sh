#!/bin/sh

. ${GCDIR-.}/scripts/run-base.sh
. ${GCDIR-.}/scripts/prop-helper.sh

run_parameter () {
  run_base "$@"; status=$?
  if [ $status -ne 118 ]; then exit $status; fi

  option="$1"
  shift
  case "$option" in
    span)     span "$@"; return ;;
    getfile)  getfile "$@"; return ;;
    get)      get "$@"; return ;;

    *) return 118 ;;
  esac
}

run () {
  run_parameter "$@"; exit $?
}
