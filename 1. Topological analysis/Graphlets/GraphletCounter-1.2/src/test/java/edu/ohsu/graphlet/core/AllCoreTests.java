package edu.ohsu.graphlet.core;

import org.junit.runners.Suite;
import org.junit.runner.RunWith;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        GraphletCounterTest.class,
        DirectedGraphletCounterTest.class,
        MotifInstanceCollapserTest.class
        })
public class AllCoreTests {
    public static Test suite() {
         return new JUnit4TestAdapter(AllCoreTests.class);
     }
}
