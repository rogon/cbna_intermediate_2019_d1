package edu.ohsu.graphlet.fileio;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

import edu.ohsu.graphlet.core.Network;
import edu.ohsu.graphlet.core.Node;
import edu.ohsu.graphlet.fileio.SimpleGraphFileReader;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */
public class SimpleGraphFileReaderTest {

    @Test
    public void testReadMembersFile() throws IOException {
        String fileContent =
                "a b 1\n" +
                        "b c 1\n" +
                        "a c 1\n" +
                        "c e 1\n" +
                        "e f 1\n" +
                        "e g 1\n" +
                        "e h 1\n" +
                        "e i 1\n" +
                        "e j 1\n" +
                        "e k 1\n" +
                        "k l 1\n";

        StringReader reader = new StringReader(fileContent);

        SimpleGraphFileReader fileReader = new SimpleGraphFileReader();
        Network network = fileReader.readNetworkFile(reader);
        assertNotNull(network);
        assertNotNull(network.getNode("a"));
        Node a = network.getNode("a");
        Node b = network.getNode("b");
        assertTrue(a.getNeighbors().contains(b));
        assertTrue(b.getNeighbors().contains(a));
        Node c = network.getNode("c");
        assertTrue(b.getNeighbors().contains(c));
        Node e = network.getNode("e");
        assertEquals(7, e.getNeighbors().size());
    }



}
