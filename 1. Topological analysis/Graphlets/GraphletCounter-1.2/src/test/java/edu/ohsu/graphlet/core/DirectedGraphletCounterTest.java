package edu.ohsu.graphlet.core;

import org.junit.Before;
import org.junit.Test;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DirectedGraphletCounterTest {
    private DirectedGraphletCounter counter;
    private static final int NUM_ORBITS = 73;

    @Before
    public void setup() {
        counter = new DirectedGraphletCounter();
    }

    @Test
    public void testG0() {
        Node a = new DirectedNodeImpl("a");

        int[] aSpectrum = new int[NUM_ORBITS];

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

        Node b = new DirectedNodeImpl("b");
        a.addNeighbor(b);
        aSpectrum[0] = 1;

        results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);
        
        b.addNeighbor(a);
        results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

    }

    @Test
    public void testG1() {
        Node a = new DirectedNodeImpl("a");

        int[] aSpectrum = new int[NUM_ORBITS];
        DirectedNodeImpl b = new DirectedNodeImpl("b");
        a.addNeighbor(b);
        DirectedNodeImpl c = new DirectedNodeImpl("c");
        c.addNeighbor(b);

        aSpectrum[0] = 1;
        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);
        c.getNeighbors().remove(b);
        b.addNeighbor(c);
        aSpectrum[1] = 1;
        results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

        b.getNeighbors().remove(c);
        a.getNeighbors().add(c);
        aSpectrum[0] = 2;
        aSpectrum[1] = 0;
        aSpectrum[2] = 1;

        results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

    }


    @Test
    public void testG2() {
        Node a = new DirectedNodeImpl("a");

        int[] aSpectrum = new int[NUM_ORBITS];
        DirectedNodeImpl b = new DirectedNodeImpl("b");
        a.addNeighbor(b);
        DirectedNodeImpl c = new DirectedNodeImpl("c");
        b.addNeighbor(c);
        c.addNeighbor(a);

        aSpectrum[0] = 1;
        aSpectrum[3] = 1;

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);
        results = counter.getGraphletSignature(b);
        GraphletCounterTest.assertSame(aSpectrum, results);
    }

    @Test
    public void testG3() {
        Node a = new DirectedNodeImpl("a");
        Node b = new DirectedNodeImpl("b");
        Node c = new DirectedNodeImpl("c");
        Node d = new DirectedNodeImpl("d");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);

        int[] aSpectrum = new int[NUM_ORBITS];

        aSpectrum[0] = 1;
        aSpectrum[1] = 1;
        aSpectrum[4] = 1;

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

        b.addNeighbor(a);

        int[] bSpectrum = new int[NUM_ORBITS];

        bSpectrum[0] = 2;
        bSpectrum[1] = 1;
        bSpectrum[2] = 1;
        bSpectrum[5] = 1;

        results = counter.getGraphletSignature(b);
        GraphletCounterTest.assertSame(bSpectrum, results);

    }

    @Test
    public void testG4() {
        Node a = new DirectedNodeImpl("a");
        Node b = new DirectedNodeImpl("b");
        Node c = new DirectedNodeImpl("c");
        Node d = new DirectedNodeImpl("d");

        a.addNeighbor(b);
        a.addNeighbor(c);
        a.addNeighbor(d);

        int[] aSpectrum = new int[NUM_ORBITS];

        aSpectrum[0] = 3;
        aSpectrum[2] = 3;
        aSpectrum[7] = 1;

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

        b.addNeighbor(a);
        int[] bSpectrum = new int[NUM_ORBITS];
        bSpectrum[0] = 1;
        bSpectrum[1] = 2;
        bSpectrum[6] = 1;

        results = counter.getGraphletSignature(b);
        GraphletCounterTest.assertSame(bSpectrum, results);
    }

    @Test
    public void testG5() {
        Node a = new DirectedNodeImpl("a");
        Node b = new DirectedNodeImpl("b");
        Node c = new DirectedNodeImpl("c");
        Node d = new DirectedNodeImpl("d");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(a);

        int[] aSpectrum = new int[NUM_ORBITS];

        aSpectrum[0] = 1;
        aSpectrum[1] = 1;
        aSpectrum[8] = 1;

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

    }

    @Test
    public void testG6() {
        Node a = new DirectedNodeImpl("a");
        Node b = new DirectedNodeImpl("b");
        Node c = new DirectedNodeImpl("c");
        Node d = new DirectedNodeImpl("d");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(b);

        b.addNeighbor(a);
        b.addNeighbor(d);

        c.addNeighbor(b);

        int[] aSpectrum = new int[NUM_ORBITS];

        aSpectrum[0] = 1;
        aSpectrum[1] = 2;
        aSpectrum[9] = 1;

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

        int[] bSpectrum = new int[NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[2] = 2;
        bSpectrum[3] = 1;
        bSpectrum[11] = 1;

        results = counter.getGraphletSignature(b);
        GraphletCounterTest.assertSame(bSpectrum, results);

        int[] cSpectrum = new int[NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 2;
        cSpectrum[3] = 1;
        cSpectrum[6] = 1; // todo: this is weird??
        cSpectrum[10] = 1;

        results = counter.getGraphletSignature(c);
        GraphletCounterTest.assertSame(cSpectrum, results);

    }

    @Test
    public void testG7() {
        Node a = new DirectedNodeImpl("a");
        Node b = new DirectedNodeImpl("b");
        Node c = new DirectedNodeImpl("c");
        Node d = new DirectedNodeImpl("d");

        a.addNeighbor(b);
        a.addNeighbor(c);
        a.addNeighbor(d);
        b.addNeighbor(c);
        d.addNeighbor(c);

        int[] aSpectrum = new int[NUM_ORBITS];

//        aSpectrum[0] = 3;
//        aSpectrum[1] = 2;
//        aSpectrum[2] = 2;
//        aSpectrum[5] = 2;
//        aSpectrum[13] = 1;
//
//        int[] results = counter.getGraphletSignature(a);
//        GraphletCounterTest.assertSame(aSpectrum, results);

        a.getNeighbors().clear();
        b.getNeighbors().clear();
        c.getNeighbors().clear();
        d.getNeighbors().clear();

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        c.addNeighbor(a);
        d.addNeighbor(a);

        aSpectrum[0] = 1;
        aSpectrum[3] = 1;
        aSpectrum[13] = 1;

        int[] results = counter.getGraphletSignature(a);
        GraphletCounterTest.assertSame(aSpectrum, results);

    }

}
