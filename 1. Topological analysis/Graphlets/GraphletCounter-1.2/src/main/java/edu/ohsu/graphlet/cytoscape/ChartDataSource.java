package edu.ohsu.graphlet.cytoscape;

import jas.hist.DataSource;
import jas.hist.XYDataSource;

/**
 * Model backing the chart.
 */
class ChartDataSource implements XYDataSource {
    private final double[] means;
    private String title;

    public ChartDataSource(double[] means, String title) {
        this.means = means;
        this.title = title;
    }

    public int getNPoints() {
        return means.length;
    }

    public double getX(int i) {
        return i;
    }

    public double getY(int i) {
        return means[i];
    }

    public double getPlusError(int i) {
        return 0;
    }

    public double getMinusError(int i) {
        return 0;
    }

    public int getAxisType() {
        return DataSource.DOUBLE;
    }

    public String getTitle() {
        return title;
    }
}
