package edu.ohsu.graphlet.cytoscape;

import cytoscape.CyNetwork;
import cytoscape.CyNode;
import cytoscape.Cytoscape;
import cytoscape.task.TaskMonitor;
import edu.ohsu.graphlet.core.*;

import java.util.*;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Cytoscape task for counting graphlet signatures for a list of connected node sets, or motifs.
 */
public class CountGraphletsForMotifsTask extends GraphletCountTask {

    private TaskMonitor taskMonitor = null;
    private CyNetwork currentNetwork;
    private boolean interrupted;
    private GraphletCountResults results;
    private List<Motif> motifList;

    /**
     * Create a new task to compute the graphlet signatures of the motifs in Motif given the CytoscapeNetwork
     * @param motifList
     * @param currentNetwork
     */
    public CountGraphletsForMotifsTask(List<Motif> motifList, CyNetwork currentNetwork) {
        this.motifList = motifList;
        this.currentNetwork = currentNetwork;
    }

    public GraphletCountResults getResults() {
        return results;
    }

    /**
     * Executes the graphlet counting task and places the results in results.
     */
    public void run() {
        CyNetwork workspace = Cytoscape.createNetwork(currentNetwork.getNodeIndicesArray(), currentNetwork.getEdgeIndicesArray(), "Workspace");
        CountResults countsByMotifInstance = new MotifCountResults();
        MotifInstanceCollapser motifInstanceCollapser = new MotifInstanceCollapser();
        GraphletCounter counter = new GraphletCounter();
        for (Motif motif : motifList) {
            for (MotifInstance motifInstance : motif.getInstances()) {
                if (interrupted) return;
                Map<CyNode, int[]> edgesByNodes = new HashMap<CyNode, int[]>();
                for (String node : motifInstance.getMembers()) {
                    CyNode originalNode = Cytoscape.getCyNode(node);
                    edgesByNodes.put(originalNode, workspace.getAdjacentEdgeIndicesArray(workspace.getIndex(originalNode), true, true, true));
                }
                taskMonitor.setStatus("Counting " + motifInstance.getMotifInstanceName());
                CyNetworkImpl networkToWorkOn = new CyNetworkImpl(workspace);
                motifInstanceCollapser.collapseNodes(motifInstance, networkToWorkOn);
                motifInstance.setCounts(counter.getGraphletSignature(new CyNodeImpl(workspace, Cytoscape.getCyNode(motifInstance.getMotifInstanceName()))));
                countsByMotifInstance.put(motifInstance.getId(), motifInstance);


                CyNode motifNode = Cytoscape.getCyNode(motifInstance.getMotifInstanceName());

                int[] motifNodeIndicesArray = workspace.getAdjacentEdgeIndicesArray(workspace.getIndex(motifNode), true, true, true);
                for (int i = 0; i < motifNodeIndicesArray.length; i++) {
                    workspace.removeEdge(motifNodeIndicesArray[i], true);

                }
                workspace.removeNode(workspace.getIndex(motifNode), true);
                for (String nodeName : motifInstance.getMembers()) {
                    CyNode hiddenNode = Cytoscape.getCyNode(nodeName);
                    workspace.restoreNode(hiddenNode);
                    workspace.restoreEdges(edgesByNodes.get(hiddenNode));
                }
            }
        }

        results = new GraphletCountResults();
        results.setCountsByNode(countsByMotifInstance);

        double[] meanCounts = calculateMeanCounts(countsByMotifInstance.values());
        results.setMeans(meanCounts);
        Cytoscape.destroyNetwork(workspace);
    }

    public void halt() {
        interrupted = true;
    }

    public void setTaskMonitor(TaskMonitor taskMonitor) throws IllegalThreadStateException {
        if (this.taskMonitor != null) {
            throw new IllegalStateException("Task Monitor is already set.");
        }
        this.taskMonitor = taskMonitor;
    }

    public String getTitle() {
        return "Counting Graphlets";
    }

}
