package edu.ohsu.graphlet.fileio;

/**
 * Created by IntelliJ IDEA.
 * User: cwhelan
 * Date: 3/20/11
 * Time: 9:47 PM
 */

/**
 * Thrown when the motif file does not fit the expected format.
 */
public class MotifFileFormatException extends Exception {
    public MotifFileFormatException() {
        super();
    }

    public MotifFileFormatException(String message) {
        super(message);
    }

    public MotifFileFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public MotifFileFormatException(Throwable cause) {
        super(cause);
    }
}
