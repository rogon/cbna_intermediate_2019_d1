package edu.ohsu.graphlet.core;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class for collapsing a set of connected nodes into a single node, which will have the same set of adjacent nodes
 * as the set of nodes in MotifInstance
 */
public class MotifInstanceCollapser {

    /**
     * Updates network by replacing all of the nodes in MotifInstance with a single node, and adding and removing
     * edges in the network so that the new nodes has the same adjacency set as union of the adjecency sets of nodes
     * in motifInstance
     * @param motifInstance
     * @param network
     */
    public void collapseNodes(MotifInstance motifInstance, Network network) {
        Node newNode = network.createNode(motifInstance.getMotifInstanceName());

        for (String nodeId : motifInstance.getMembers()) {
            Node node = network.getNode(nodeId);
            if (node == null) {
                throw new IllegalArgumentException("Bad motif membership " + motifInstance.getMotifInstanceName() + "; node " + nodeId + " does not seem to exist in the network.");
            }
            for (Node n : node.getNeighbors()) {
                if (! n.equals(newNode) && ! motifInstance.containsNode(n.getId())) {
                    newNode.addNeighbor(n);
                }
            }
            network.hideNode(node);
        }
    }

}
