package edu.ohsu.graphlet.cytoscape;

import cytoscape.CyNetwork;
import cytoscape.Cytoscape;
import cytoscape.CyEdge;
import cytoscape.data.Semantics;

import java.util.HashSet;
import java.util.Set;

import edu.ohsu.graphlet.core.BaseGraphletSignature;
import edu.ohsu.graphlet.core.Node;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * An implementatinon of Node backed by a giny.model.Node that exists within a CyNetwork.
 */
public class CyNodeImpl extends BaseGraphletSignature implements Node {

    private CyNetwork network;
    private giny.model.Node cyNode;

    /**
     * The set of nodes that are adjacent to this node. May be cached.
     */
    Set<Node> neighbors;

    public CyNodeImpl(CyNetwork network, giny.model.Node cyNode) {
        this.network = network;
        this.cyNode = cyNode;        
    }

    giny.model.Node getCyNode() {
        return cyNode;
    }

    /**
     * Uses the root graph index of the node in the cytoscape network as its unique identifier.
     * @return
     */
    public String getId() {
        return String.valueOf(cyNode.getRootGraphIndex());
    }

    /**
     * Returns the set of nodes that are adjacent to the node in the CyNetwork, treating the network as if it were
     * undirected (a link from a->b is equivalent to b->a)
     * @return
     */
    public Set<Node> getNeighbors() {
        if (neighbors == null) {
            int[] adjEdges = network.getAdjacentEdgeIndicesArray(cyNode.getRootGraphIndex(), true, true, true);
            neighbors = new HashSet<Node>();
            if (adjEdges != null) {
                for (int i = 0; i < adjEdges.length; i++) {
                    int adjEdge = adjEdges[i];
                    int source = network.getEdgeSourceIndex(adjEdge);
                    int target = network.getEdgeTargetIndex(adjEdge);
                    if (source == cyNode.getRootGraphIndex() && target != cyNode.getRootGraphIndex()) {
                        neighbors.add(new CyNodeImpl(network, network.getNode(target)));
                    } else if (target == cyNode.getRootGraphIndex() && source != cyNode.getRootGraphIndex())  {
                        neighbors.add(new CyNodeImpl(network, network.getNode(source)));
                    }
                }
            }
        }
        return neighbors;
    }

    /**
     * Adds an undirected connection between this node and n, using a Cytoscape "pp" type interaction
     * @param n
     */
    public void addNeighbor(Node n) {
        resetNeighors();
        ((CyNodeImpl) n).resetNeighors();
        network.addEdge(Cytoscape.getCyEdge(this.getCyNode(), ((CyNodeImpl) n).getCyNode(), Semantics.INTERACTION, "pp", true));        
    }

    /**
     * Clears the cache of neighbors for the node.
     */
    private void resetNeighors() {
        neighbors = null;
    }

    /**
     * Not implemented in current version, use Network.hideNode() instead
     * @param neighbor
     */
    public void removeEdge(Node neighbor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CyNodeImpl)) return false;

        CyNodeImpl cyNode1 = (CyNodeImpl) o;

        if (getId() == null) return cyNode1.getId() == null;

        return getId().equals(cyNode1.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
