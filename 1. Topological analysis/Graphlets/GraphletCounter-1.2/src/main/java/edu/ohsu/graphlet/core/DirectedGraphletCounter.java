package edu.ohsu.graphlet.core;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class is under development and not in use yet!!
 *
 * This class computes graphlet counts for directed graphs.
 */
public class DirectedGraphletCounter extends GraphletCounter {

    protected void adjustForOvercounting(int[] results) {
        results[1] = results[1];
        results[2] = results[2]/2;
        results[3] = results[3];
        results[4] = results[4];
        results[5] = results[5];
        results[6] = results[6]/2;
        results[7] = results[7]/6;
        results[8] = results[8];
        results[9] = results[9];
        results[10] = results[10];
        results[11] = results[11];
        results[12] = results[12]/2;
        results[13] = results[13];
        results[14] = results[14]/6;
        results[15] = results[15];
        results[16] = results[16];
        results[17] = results[17]/2;
        results[18] = results[18]/2;
        results[19] = results[19];
        results[20] = results[20]/2;
        results[21] = results[21]/2;
        results[22] = results[22]/6;
        results[23] = results[23]/24;
        results[24] = results[24];
        results[25] = results[25]/2;
        results[26] = results[26];
        results[27] = results[27]/2;
        results[28] = results[28]/2;
        results[29] = results[29];
        results[30] = results[30]/2;
        results[31] = results[31]/2;
        results[32] = results[32]/2;
        results[33] = results[33]/4;
        results[34] = results[34]/2;
        results[35] = results[35]/2;
        results[36] = results[36]/2;
        results[37] = results[37];
        results[38] = results[38]/2;
        results[39] = results[39]/2;
        results[40] = results[40];
        results[41] = results[41]/2;
        results[42] = results[42]/2;
        results[43] = results[43]/2;
        results[44] = results[44]/8;
        results[45] = results[45]/2;
        results[46] = results[46]/2;
        results[47] = results[47]/2;
        results[48] = results[48];
        results[49] = results[49]/4;
        results[50] = results[50]/6;
        results[51] = results[51]/3;
        results[52] = results[52]/4;
        results[53] = results[53]/2;
        results[54] = results[54]/4;
        results[55] = results[55]/6;
        results[56] = results[56]/6;
        results[57] = results[57]/2;
        results[58] = results[58]/6;
        results[59] = results[59];
        results[60] = results[60];
        results[61] = results[61]/2;
        results[62] = results[62]/4;
        results[63] = results[63]/2;
        results[64] = results[64]/2;
        results[65] = results[65]/4;
        results[66] = results[66]/2;
        results[67] = results[67]/2;
        results[68] = results[68]/2;
        results[69] = results[69]/8;
        results[70] = results[70]/6;
        results[71] = results[71]/4;
        results[72] = results[72] / (4 * 3 * 2);
    }

}
