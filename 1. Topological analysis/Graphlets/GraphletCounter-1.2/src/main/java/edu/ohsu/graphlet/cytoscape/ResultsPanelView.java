package edu.ohsu.graphlet.cytoscape;

import cytoscape.Cytoscape;
import cytoscape.view.cytopanels.CytoPanel;
import edu.ohsu.graphlet.core.GraphletCounter;
import edu.ohsu.graphlet.core.HasGraphletSignature;
import jas.hist.JASHist;
import jas.hist.JASHistData;
import jas.hist.XYDataSource;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: cwhelan
 * Date: 10/18/11
 * Time: 2:56 PM
 */
public class ResultsPanelView {

    public static final String MEAN_COUNT_LABEL = "Mean Count";
    public static final String MEAN_COUNT_LABEL_FOR_MOTIFS = "Mean Count (All Subgraphs)";
    static NumberFormat format = NumberFormat.getInstance();
    // results panel stuff
    private JTable resultsTable;
    private Panel resultsPanel;
    private JComboBox nodeSelectionBox;
    private JASHist jasHist;
    private JASHistData histData;
    private JRadioButton rawCountButton;
    private JRadioButton weightedCountButton;
    private JFileChooser outputFileChooser;
    private JPanel controlPanel;
    private JButton saveButton;
    private JButton closeButton;
    private JPanel dataPanel;
    private JPanel subgraphTypePanel;
    private JComboBox subgraphFilter;
    private JDialog orbitDictionaryWindow;

    public JTable getResultsTable() {
        return resultsTable;
    }

    public void setResultsTable(JTable resultsTable) {
        this.resultsTable = resultsTable;
    }

    public Panel getResultsPanel() {
        return resultsPanel;
    }

    public void setResultsPanel(Panel resultsPanel) {
        this.resultsPanel = resultsPanel;
    }

    public JComboBox getNodeSelectionBox() {
        return nodeSelectionBox;
    }

    public void setNodeSelectionBox(JComboBox nodeSelectionBox) {
        this.nodeSelectionBox = nodeSelectionBox;
    }

    public JASHist getJasHist() {
        return jasHist;
    }

    public void setJasHist(JASHist jasHist) {
        this.jasHist = jasHist;
    }

    public JASHistData getHistData() {
        return histData;
    }

    public void setHistData(JASHistData histData) {
        this.histData = histData;
    }

    public JRadioButton getRawCountButton() {
        return rawCountButton;
    }

    public void setRawCountButton(JRadioButton rawCountButton) {
        this.rawCountButton = rawCountButton;
    }

    public JRadioButton getWeightedCountButton() {
        return weightedCountButton;
    }

    public void setWeightedCountButton(JRadioButton weightedCountButton) {
        this.weightedCountButton = weightedCountButton;
    }

    public JFileChooser getOutputFileChooser() {
        return outputFileChooser;
    }

    public void setOutputFileChooser(JFileChooser outputFileChooser) {
        this.outputFileChooser = outputFileChooser;
    }

    public JPanel getControlPanel() {
        return controlPanel;
    }

    public void setControlPanel(JPanel controlPanel) {
        this.controlPanel = controlPanel;
    }

    public JButton getSaveButton() {
        return saveButton;
    }

    public void setSaveButton(JButton saveButton) {
        this.saveButton = saveButton;
    }

    public JButton getCloseButton() {
        return closeButton;
    }

    public void setCloseButton(JButton closeButton) {
        this.closeButton = closeButton;
    }

    /**
     * Given an array of mean counts, returns an array of the same size holding the natural logs of the mean counts.
     *
     * @param x
     * @return
     */
    double[] computeLogs(double[] x) {
        double[] logCounts = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            logCounts[i] = Math.log(x[i] + 1);
        }
        return logCounts;
    }

    /**
     * Sets the range on the histogram display when means are being displayed
     * @param means
     */
    protected void setHistogramRangeForMeans(double[] means) {
        double[] values = computeLogs(means);
        double maxValue = 0;
        for (double v : values) {
            if (v > maxValue) maxValue = v;
        }
        jasHist.getYAxis().setRange(0, Math.ceil(maxValue));
    }

    /**
     * Sets the range on the histogram display
     * @param x
     */
    protected void setHistogramRange(int[] x) {
        double[] values = computeLogs(x);
        double maxValue = 0;
        for (double v : values) {
            if (v > maxValue) maxValue = v;
        }
        jasHist.getYAxis().setRange(0, Math.ceil(maxValue));
    }

    /**
     * Given an array of integer counts, returns an array of the same size holding the natural logs of the counts.
     *
     * @param x
     * @return
     */
    double[] computeLogs(int[] x) {
        double[] logCounts = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            logCounts[i] = Math.log(x[i] + 1);
        }
        return logCounts;
    }

    /**
     * Returns the currently selected node id
     * @return
     */
    String getSelectedNode() {
        return (String) getNodeSelectionBox().getSelectedItem();
    }

    /**
     * Builds the combo box that lets you choose which graphlet counts to display in the results panel, i.e.
     * for a single node or the mean of all nodes.
     *
     * @return
     * @param nodeSelectionOptions
     * @param forMotifs
     */
    ComboBoxModel buildComboBoxModel(Set<String> nodeSelectionOptions, boolean forMotifs) {
        java.util.List<String> nodeList = new ArrayList<String>();
        for (String node : nodeSelectionOptions) {
            nodeList.add(node);
        }
        Collections.sort(nodeList);
        if (nodeSelectionOptions.size() > 1) {
            java.util.List tempList = new ArrayList<String>();
            tempList.add(forMotifs ? MEAN_COUNT_LABEL_FOR_MOTIFS : MEAN_COUNT_LABEL);
            tempList.addAll(nodeList);
            nodeList = tempList;
        }
        return new DefaultComboBoxModel(nodeList.toArray());
    }

    /**
     * Builds the combo box that lets you choose which subgraph ids you would like to view
     *
     * @param subgraphIds
     * @return
     */
    ComboBoxModel buildComboBoxModelForSubgraphFilter(Set<String> subgraphIds) {
        java.util.List<String> subgraphIdList = new ArrayList<String>();
        for (String subgraphId : subgraphIds) {
            subgraphIdList.add(subgraphId);
        }
        Collections.sort(subgraphIdList);
        subgraphIdList.add(0, "All");
        return new DefaultComboBoxModel(subgraphIdList.toArray());
    }

    /**
     * Populates the nodes that are selectable
     * @param nodeSelectionOptions
     * @param isForMotifs
     */
    void populateNodeOptions(Set<String> nodeSelectionOptions, boolean isForMotifs) {
        ComboBoxModel comboBoxModel = buildComboBoxModel(nodeSelectionOptions, isForMotifs);
        if (getNodeSelectionBox() == null) {
            setNodeSelectionBox(new JComboBox(comboBoxModel));
            getNodeSelectionBox().setLightWeightPopupEnabled(false);
        }

        getNodeSelectionBox().setModel(comboBoxModel);
    }

    /**
     * Selects the node at the specified index in the node selection list
     * @param index
     */
    void setSelectedNodeIndex(int index) {
        getNodeSelectionBox().setSelectedIndex(index);
    }

    /**
     * Sets up the data source for the results panel chart.
     *
     * @param means
     * @param title
     * @return
     */
    XYDataSource buildChartDataSource(final double[] means, String title) {
        return new ChartDataSource(means, title);
    }

    /**
     * Builds the model for the results panel graphlet count table for the mean results for each orbit.
     *
     *
     * @param countResults
     * @return
     */
    TableModel buildTableModelForMeanCounts(final GraphletCountResults countResults) {
        return new AbstractTableModel() {
            public int getRowCount() {
                return GraphletCounter.NUM_ORBITS;
            }

            public int getColumnCount() {
                return 3;
            }

            public Object getValueAt(int row, int col) {
                switch (col) {
                    case 0:
                        return row;
                    case 1:
                        return format.format(countResults.getMeans()[row]);
                    case 2:
                        return format.format(countResults.getWeightedMeans()[row]);
                }
                return null;
            }

            @Override
            public String getColumnName(int col) {
                switch (col) {
                    case 0:
                        return "Orbit";
                    case 1:
                        return "Degree";
                    case 2:
                        return "Weighted Degree";
                }
                return null;
            }

        };

    }

    /**
     * Builds the model for the results panel graphlet count table for results for an individual node.
     *
     * @param node
     * @return
     */
    protected TableModel buildTableModelForResults(final HasGraphletSignature node) {
        return new AbstractTableModel() {

            private DecimalFormat decimalFormat = new DecimalFormat("#.#####");

            public int getRowCount() {
                return GraphletCounter.NUM_ORBITS;
            }

            public int getColumnCount() {
                return 3;
            }

            public Object getValueAt(int row, int col) {
                switch (col) {
                    case 0:
                        return row;
                    case 1:
                        return node.getCounts()[row];
                    case 2:
                        return decimalFormat.format(node.getWeightedCounts()[row]);
                }
                return null;
            }

            @Override
            public String getColumnName(int col) {
                switch (col) {
                    case 0:
                        return "Orbit";
                    case 1:
                        return "Degree";
                    case 2:
                        return "Weighted Degree";
                }
                return null;
            }
        };
    }

    /**
     * Setup the initial data in the data table
     * @param results
     */
    void populateInitialResultsTable(GraphletCountResults results) {
        TableModel tableModel;
        if (results.getCountsByNodeId().keySet().size() > 1) {
            tableModel = buildTableModelForMeanCounts(results);
        } else {
            HasGraphletSignature node = results.getCountsByNodeId().values().iterator().next();
            tableModel = buildTableModelForResults(node);
        }
        if (getResultsTable() == null) {
            setResultsTable(new JTable(tableModel));
        }
        getResultsTable().setModel(tableModel);
        getResultsTable().repaint();
    }

    /**
     * Create and lay out the UI components that make up the results panel
     * @param cytoPanel
     */
    void buildResultsPanel(CytoPanel cytoPanel) {
        resultsPanel = new Panel(new BorderLayout());

        JPanel selectionPanel = new JPanel();
        selectionPanel.setLayout(new BoxLayout(selectionPanel, BoxLayout.PAGE_AXIS));
        if (nodeSelectionBox == null) {
            nodeSelectionBox = new JComboBox();
            nodeSelectionBox.setLightWeightPopupEnabled(false);
        }
        subgraphTypePanel = new JPanel();
        subgraphTypePanel.setLayout(new BoxLayout(subgraphTypePanel, BoxLayout.LINE_AXIS));
        subgraphTypePanel.add(new JLabel("Show motifs from subgraph: "));
        subgraphFilter = new JComboBox();
        JButton subgraphHelpLink = new JButton();
        subgraphHelpLink.putClientProperty("JButton.buttonType", "help");
        subgraphHelpLink.setIcon(new ImageIcon("question1.png"));
        subgraphHelpLink.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Desktop.isDesktopSupported()) {
                    Desktop desktop = Desktop.getDesktop();
                    try {
                        desktop.browse(new URI("http://www.weizmann.ac.il/mcb/UriAlon/NetworkMotifsSW/mfinder/motifDictionary.pdf"));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                }
            }
        });
        subgraphTypePanel.add(subgraphFilter);
        subgraphTypePanel.add(subgraphHelpLink);
        subgraphTypePanel.setVisible(false);
        selectionPanel.add(subgraphTypePanel);
        selectionPanel.add(nodeSelectionBox);

        setResultsTable(new JTable());
        JScrollPane resultsTableScrollPanel = new JScrollPane(getResultsTable());
        setJasHist(new JASHist());
        dataPanel = new JPanel();
        dataPanel.setLayout(new BoxLayout(dataPanel, BoxLayout.PAGE_AXIS));
        dataPanel.add(resultsTableScrollPanel);
        final ButtonGroup buttonGroup = new ButtonGroup();
        setRawCountButton(new JRadioButton("Log degree"));
        buttonGroup.add(getRawCountButton());
        setWeightedCountButton(new JRadioButton("Log weighted degree"));
        buttonGroup.add(getWeightedCountButton());
        getRawCountButton().setSelected(true);

        Box weightingButtons = new Box(BoxLayout.LINE_AXIS);
        weightingButtons.add(rawCountButton);
        weightingButtons.add(weightedCountButton);

        dataPanel.add(weightingButtons);
        dataPanel.add(getJasHist());
        JButton orbitHelpButton = new JButton();
        orbitHelpButton.setText("Orbit Dictionary");

        JButton graphletCounterHelp = new JButton("Help");
        graphletCounterHelp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Desktop.isDesktopSupported()) {
                    Desktop desktop = Desktop.getDesktop();
                    try {
                        desktop.browse(new URI("http://sonmezsysbio.org/software/graphletcounter/graphletcounter-readme"));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                }
            }
        });


        Box helpBox = new Box(BoxLayout.LINE_AXIS);

        helpBox.add(orbitHelpButton);
        helpBox.add(graphletCounterHelp);
        dataPanel.add(helpBox);


        try {
            BufferedImage orbitDictionary = ImageIO.read(this.getClass().getResource("orbitDictionary.jpg"));
            JLabel dictionaryPanel = new JLabel(new ImageIcon(orbitDictionary));
            orbitDictionaryWindow = new JDialog(Cytoscape.getDesktop());
            JTextArea caption = new JTextArea("The thirty 2-, 3-, 4-, and 5-node graphlets G0, G1, …, G29 " +
                    "and their automorphism orbits 0, 1, 2, …, 72. " +
                    "Nodes belonging to the same orbit are of the same shade.\n" +
                    "Figure from Milenkovic and Przulj, Cancer Informatics 2008:6 257–273.");
            caption.setLineWrap(true);
            caption.setEditable(false);
            orbitDictionaryWindow.add(BorderLayout.NORTH, caption);
            orbitDictionaryWindow.add(BorderLayout.CENTER, dictionaryPanel);
            JButton closeButton = new JButton("Close");
            closeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    orbitDictionaryWindow.setVisible(false);
                }
            });
            orbitDictionaryWindow.add(BorderLayout.SOUTH, closeButton);
            orbitDictionaryWindow.setSize(600, 480);
            orbitDictionaryWindow.setResizable(false);

            orbitDictionaryWindow.setVisible(false);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        orbitHelpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                orbitDictionaryWindow.setVisible(true);
            }
        });

        outputFileChooser = new JFileChooser();

        controlPanel = new JPanel();
        saveButton = new JButton("Save");
        closeButton = new JButton("Close");

        controlPanel.add(saveButton);
        controlPanel.add(closeButton);

        resultsPanel.add(BorderLayout.NORTH, selectionPanel);
        resultsPanel.add(BorderLayout.CENTER, dataPanel);
        resultsPanel.add(BorderLayout.SOUTH, controlPanel);
        cytoPanel.add("GraphletCounter", this.resultsPanel);


    }

    /**
     * Populate the data table with mean data from the graphlet count results
     * @param results
     */
    void displayMeanData(GraphletCountResults results) {
        getResultsTable().setModel(buildTableModelForMeanCounts(results));
        getHistData().show(false);
        getJasHist().removeAllData();
        setHistData(getJasHist().addData(buildChartDataSource(computeLogs(results.getMeans()), "Mean graphlet counts")));
        setHistogramRangeForMeans(results.getMeans());
        getHistData().show(true);
    }

    /**
     * Populate the data table with graphlet counts for the given node
     * @param selection
     * @param results
     */
    void displayNodeResults(String selection, GraphletCountResults results) {
        getHistData().show(false);
        getJasHist().removeAllData();

        getResultsTable().setModel(buildTableModelForResults(results.getGraphletSignature(selection)));
        setHistData(getJasHist().addData(buildChartDataSource(computeLogs(results.getCountsByNodeId().get(selection).getCounts()), "Node " + selection)));
        setHistogramRange(results.getCountsByNodeId().get(selection).getCounts());
        getHistData().show(true);
    }


    /**
     * Given an array of mean counts, returns an array of the same size holding the natural logs of the mean counts.
     *
     * @param x
     * @return
     */
    double[] computeLogs(Double[] x) {
        double[] logCounts = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            logCounts[i] = Math.log(x[i] + 1);
        }
        return logCounts;
    }

    /**
     * Populate the histogram with mean data from the graphlet count results
     * @param results
     *
     */
    void populateHistogramDataWithMeans(GraphletCountResults results) {
        getJasHist().removeAllData();
        setHistData(getJasHist().addData(
                buildChartDataSource(computeLogs(results.getMeans()), "Mean graphlet counts")));
        setHistogramRangeForMeans(results.getMeans());
        getHistData().show(true);
    }

    /**
     * Populate the histogram with count data from the graphlet count results for the given node id
     * @param results
     *
     */
    void populateHistogramDataForNode(String selection, GraphletCountResults results) {
        getJasHist().removeAllData();
        setHistData(getJasHist().addData(
                buildChartDataSource(computeLogs(results.getCountsByNodeId().get(selection).getCounts()), "Node " + selection)));
        setHistogramRange(results.getCountsByNodeId().get(selection).getCounts());
        getHistData().show(true);
    }

    /**
     * Show or hide the subgraph filter selection panel
     * @param b
     */
    public void setSubgraphFilterVisible(boolean b) {
        subgraphTypePanel.setVisible(b);
    }

    /**
     * Set the available subgraph IDs to filter on
     * @param subgraphIds
     */
    public void setSubgraphFilterOptions(Set<String> subgraphIds) {
        ComboBoxModel comboBoxModel = buildComboBoxModelForSubgraphFilter(subgraphIds);
        subgraphFilter.setModel(comboBoxModel);
    }

    public JComboBox getSubgraphFilter() {
        return subgraphFilter;
    }

    public String getSelectedSubgraphId() {
        return (String) subgraphFilter.getSelectedItem();
    }
}
