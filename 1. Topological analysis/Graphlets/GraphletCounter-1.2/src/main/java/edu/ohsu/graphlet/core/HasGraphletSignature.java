package edu.ohsu.graphlet.core;

/**
 * Created by IntelliJ IDEA.
 * User: cwhelan
 * Date: 10/20/11
 * Time: 12:15 PM
 */
public interface HasGraphletSignature extends Comparable<HasGraphletSignature> {

    public String getId();

    public int[] getCounts();

    public double[] getWeightedCounts();

    void setCounts(int[] counts);
}
